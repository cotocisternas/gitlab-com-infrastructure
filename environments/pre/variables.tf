variable "gitlab_io_zone_id" {}

variable "project" {
  default = "gitlab-pre"
}

variable "bootstrap_script_version" {
  default = 7
}

variable "region" {
  default = "us-east1"
}

variable "environment" {
  default = "pre"
}

variable "dns_zone_name" {
  default = "gitlab.net"
}

variable "default_kernel_version" {
  default = "4.15.0-1015"
}

variable "oauth2_client_id_dashboards" {
  default = "test"
}

variable "oauth2_client_secret_dashboards" {
  default = "test"
}

variable "oauth2_client_id_gitlab_pre" {
  default = "test"
}

variable "oauth2_client_secret_gitlab_pre" {
  default = "test"
}

variable "oauth2_client_id_monitoring" {
  default = "test"
}

variable "oauth2_client_secret_monitoring" {
  default = "test"
}

variable "machine_types" {
  type = "map"

  default = {
    "bastion"            = "n1-standard-1"
    "gitlab-pre"         = "n1-standard-16"
    "monitoring"         = "n1-standard-2"
    "sd-exporter"        = "n1-standard-1"
    "gke-pre"            = "n1-standard-1"
    "gke-runner"         = "n1-standard-2"
    "web"                = "n1-standard-1"
    "git"                = "n1-standard-1"
    "api"                = "n1-standard-1"
    "sidekiq-besteffort" = "n1-standard-1"
    "registry"           = "n1-standard-1"
    "web-pages"          = "n1-standard-1"
    "fe-lb"              = "n1-standard-1"
    "gitaly"             = "n1-standard-1"
    "deploy"             = "n1-standard-1"
  }
}

variable "monitoring_hosts" {
  type = "map"

  default = {
    "names" = ["prometheus", "prometheus-app"]
    "ports" = [9090, 9090]
  }
}

variable "service_account_email" {
  type = "string"

  default = "terraform@gitlab-pre.iam.gserviceaccount.com"
}

#############################
# Default firewall
# rule for allowing
# all protocols on all
# ports
#
# 10.232.x.x: all of pre
# 10.250.7.x: ops runner
# 10.250.10.x: chatops runner
# 10.250.12.x: release runner
# 10.12.0.0/14: pod address range in gitlab-ops for runners
###########################

variable "internal_subnets" {
  type    = "list"
  default = ["10.232.0.0/13", "10.250.7.0/24", "10.250.10.0/24", "10.250.12.0/24", "10.12.0.0/14"]
}

variable "other_monitoring_subnets" {
  type = "list"

  # 10.219.1.0/24: gprd
  # 10.251.17.0/24: dr
  # Left empty for preprod
  default = []
}

# The pre network is allocated
# 10.232.0.0/13
#   First IP: 10.232.0.0
#   Last IP:  10.239.255.255

variable "subnetworks" {
  type = "map"

  default = {
    "bastion"        = "10.232.1.0/24"
    "gitlab-pre"     = "10.232.2.0/24"
    "monitoring"     = "10.232.3.0/24"
    "pubsubbeat"     = "10.232.4.0/24"
    "gke-runner"     = "10.232.5.0/24"
    "sd-exporter"    = "10.232.6.0/24"
    "redis      "    = "10.232.7.0/24"
    "fe-lb"          = "10.232.9.0/24"
    "fe-lb-pages"    = "10.232.10.0/24"
    "fe-lb-registry" = "10.232.11.0/24"
    "registry"       = "10.232.12.0/24"
    "web"            = "10.232.13.0/24"
    "api"            = "10.232.14.0/24"
    "git"            = "10.232.15.0/24"
    "sidekiq"        = "10.232.16.0/24"
    "web-pages"      = "10.232.17.0/24"
    "gitaly"         = "10.232.18.0/24"
    "deploy"         = "10.232.19.0/24"
    "gke-pre"        = "10.232.20.0/24"
    "web-puma"       = "10.232.21.0/24"

    "gke-pre-pod-cidr"     = "10.235.0.0/16"
    "gke-pre-service-cidr" = "10.236.0.0/16"

    # /mnt/storage
    "filestore-storage" = "10.237.0.0/29"

    "gke-runner-pod-cidr"     = "10.238.0.0/16"
    "gke-runner-service-cidr" = "10.239.0.0/16"
  }
}

##################
# Network Peering
##################

variable "network_env" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-pre/global/networks/pre"
}

variable "peer_networks" {
  type = "map"

  default = {
    "names" = ["ops"]

    "links" = [
      "https://www.googleapis.com/compute/v1/projects/gitlab-ops/global/networks/ops",
    ]
  }
}

variable "public_ports" {
  type = "map"

  default = {
    "bastion"     = [22]
    "gitlab-pre"  = [443, 80, 22, 5005]
    "sd-exporter" = []
    "pubsubbeat"  = []
    "web"         = []
    "web-puma"    = [443, 80, 22]
    "api"         = []
    "git"         = []
    "sidekiq"     = []
    "registry"    = []
    "web-pages"   = []
    "fe-lb"       = [22, 80, 443]
    "gitaly"      = []
    "deploy"      = []
  }
}

variable "node_count" {
  type = "map"

  default = {
    "bastion"            = 1
    "gitlab-pre"         = 1
    "prometheus"         = 1
    "prometheus-app"     = 1
    "sd-exporter"        = 1
    "web"                = 1
    "git"                = 1
    "api"                = 1
    "sidekiq-besteffort" = 1
    "web-pages"          = 1
    "registry"           = 1
    "fe-lb"              = 2
    "fe-lb-pages"        = 1
    "fe-lb-registry"     = 1
    "gitaly"             = 1
    "deploy"             = 1
  }
}

variable "chef_provision" {
  type        = "map"
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-pre-chef-bootstrap"
    bootstrap_key     = "gitlab-pre-bootstrap-validation"
    bootstrap_keyring = "gitlab-pre-bootstrap"

    server_url    = "https://chef.gitlab.com/organizations/gitlab/"
    user_name     = "gitlab-ci"
    user_key_path = ".chef.pem"
    version       = "12.22.5"
  }
}

variable "monitoring_cert_link" {
  default = "projects/gitlab-pre/global/sslCertificates/wildcard-pre-gitlab-net"
}

variable "data_disk_sizes" {
  type = "map"

  default = {
    "file"  = "100"
    "share" = "100"
    "pages" = "100"
  }
}

variable "lb_fqdns" {
  type    = "list"
  default = ["test.pre.gitlab.com", "pre.gitlab.com"]
}

variable "lb_fqdns_bastion" {
  type    = "list"
  default = ["lb-bastion.pre.gitlab.com"]
}

variable "lb_fqdns_pages" {
  type    = "list"
  default = ["*.pages.pre.gitlab.io"]
}

variable "lb_fqdns_registry" {
  type    = "list"
  default = ["registry-test.pre.gitlab.com", "registry.pre.gitlab.com"]
}

variable "tcp_lbs_bastion" {
  type = "map"

  default = {
    "names"                  = ["ssh"]
    "forwarding_port_ranges" = ["22"]
    "health_check_ports"     = ["80"]
  }
}

variable "tcp_lbs_pages" {
  type = "map"

  default = {
    "names"                  = ["http", "https"]
    "forwarding_port_ranges" = ["80", "443"]
    "health_check_ports"     = ["8001", "8002"]
  }
}

variable "tcp_lbs_registry" {
  type = "map"

  default = {
    "names"                  = ["http", "https"]
    "forwarding_port_ranges" = ["80", "443"]
    "health_check_ports"     = ["8001", "8002"]
  }
}

variable "tcp_lbs_sentry" {
  type = "map"

  default = {
    "names"                      = ["http", "https"]
    "forwarding_port_ranges"     = ["80", "443"]
    "health_check_ports"         = ["9000", "9000"]
    "health_check_request_paths" = ["/auth/login/gitlab/", "/auth/login/gitlab/"]
  }
}

variable "tcp_lbs" {
  type = "map"

  default = {
    "names"                  = ["http", "https", "ssh"]
    "forwarding_port_ranges" = ["80", "443", "22"]
    "health_check_ports"     = ["8001", "8002", "8003"]
  }
}

variable "pre_gitlab_net_cert_link" {
  default = "projects/gitlab-pre/global/sslCertificates/pre-gitlab-net"
}

variable "gcs_service_account_email" {
  type    = "string"
  default = "gitlab-object-storage@gitlab-pre.iam.gserviceaccount.com"
}

variable "egress_ports" {
  type    = "list"
  default = ["80", "443"]
}

variable "deploy_egress_ports" {
  type    = "list"
  default = ["80", "443", "22"]
}
