## State storage
terraform {
  backend "s3" {}
}

## AWS
provider "aws" {
  region = "us-east-1"
}

variable "gitlab_com_zone_id" {}
variable "gitlab_net_zone_id" {}

## Google

provider "google" {
  version = "~> 2.6.0"
  project = "${var.project}"
  region  = "${var.region}"
}

##################################
#
#  Network
#
#################################

module "network" {
  source           = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/vpc.git?ref=v1.0.0"
  project          = "${var.project}"
  environment      = "${var.environment}"
  internal_subnets = "${var.internal_subnets}"
}

##################################
#
#  Network Peering
#
#################################

resource "google_compute_network_peering" "peering" {
  count        = "${length(var.peer_networks["names"])}"
  name         = "peering-${element(var.peer_networks["names"], count.index)}"
  network      = "${var.network_env}"
  peer_network = "${element(var.peer_networks["links"], count.index)}"
}

##################################
#
#  Monitoring
#
#  Uses the monitoring module, this
#  creates a single instance behind
#  a load balancer with identity aware
#  proxy enabled.
#
##################################

resource "google_compute_subnetwork" "monitoring" {
  ip_cidr_range            = "${var.subnetworks["monitoring"]}"
  name                     = "${format("monitoring-%v", var.environment)}"
  network                  = "${module.network.self_link}"
  private_ip_google_access = true
  project                  = "${var.project}"
  region                   = "${var.region}"
}

#######################
#
# load balancer for all hosts in this section
#
#######################

module "monitoring-lb" {
  cert_link          = "${var.monitoring_cert_link}"
  environment        = "${var.environment}"
  gitlab_net_zone_id = "${var.gitlab_net_zone_id}"
  hosts              = ["${var.monitoring_hosts["names"]}"]
  name               = "monitoring-lb"
  project            = "${var.project}"
  region             = "${var.region}"
  service_ports      = ["${var.monitoring_hosts["ports"]}"]
  source             = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-lb.git?ref=v1.0.0"
  subnetwork_name    = "${google_compute_subnetwork.monitoring.name}"
  targets            = ["${var.monitoring_hosts["names"]}"]
  url_map            = "${google_compute_url_map.monitoring-lb.self_link}"
}

#######################
module "prometheus" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-prometheus]\""
  data_disk_size        = 50
  data_disk_type        = "pd-ssd"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  fw_whitelist_subnets  = "${concat(var.monitoring_whitelist_prometheus["subnets"], var.other_monitoring_subnets)}"
  fw_whitelist_ports    = "${var.monitoring_whitelist_prometheus["ports"]}"
  machine_type          = "${var.machine_types["monitoring"]}"
  name                  = "prometheus"
  node_count            = "${var.node_count["prometheus"]}"
  oauth2_client_id      = "${var.oauth2_client_id_monitoring}"
  oauth2_client_secret  = "${var.oauth2_client_secret_monitoring}"
  persistent_disk_path  = "/opt/prometheus"
  project               = "${var.project}"
  public_ports          = ["22"]
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_path          = "/graph"
  service_port          = "${element(var.monitoring_hosts["ports"], index(var.monitoring_hosts["names"], "prometheus"))}"
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v1.0.4"
  subnetwork_name       = "${google_compute_subnetwork.monitoring.name}"
  tier                  = "inf"
  use_external_ip       = true
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

module "prometheus-app" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-prometheus-app]\""
  data_disk_size        = 50
  data_disk_type        = "pd-ssd"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  fw_whitelist_subnets  = "${concat(var.monitoring_whitelist_prometheus["subnets"], var.other_monitoring_subnets)}"
  fw_whitelist_ports    = "${var.monitoring_whitelist_prometheus["ports"]}"
  machine_type          = "${var.machine_types["monitoring"]}"
  name                  = "prometheus-app"
  node_count            = "${var.node_count["prometheus-app"]}"
  oauth2_client_id      = "${var.oauth2_client_id_monitoring}"
  oauth2_client_secret  = "${var.oauth2_client_secret_monitoring}"
  persistent_disk_path  = "/opt/prometheus"
  project               = "${var.project}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_path          = "/graph"
  service_port          = "${element(var.monitoring_hosts["ports"], index(var.monitoring_hosts["names"], "prometheus-app"))}"
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v1.0.4"
  subnetwork_name       = "${google_compute_subnetwork.monitoring.name}"
  tier                  = "inf"
  use_external_ip       = true
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

module "sd-exporter" {
  additional_scopes         = ["https://www.googleapis.com/auth/monitoring"]
  allow_stopping_for_update = true
  bootstrap_version         = 6
  chef_provision            = "${var.chef_provision}"
  chef_run_list             = "\"role[${var.environment}-infra-sd-exporter]\""
  create_backend_service    = false
  dns_zone_name             = "${var.dns_zone_name}"
  environment               = "${var.environment}"
  machine_type              = "${var.machine_types["sd-exporter"]}"
  name                      = "sd-exporter"
  node_count                = "${var.node_count["sd-exporter"]}"
  project                   = "${var.project}"
  public_ports              = "${var.public_ports["sd-exporter"]}"
  region                    = "${var.region}"
  service_account_email     = "${var.service_account_email}"
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.3"
  subnetwork_name           = "${google_compute_subnetwork.monitoring.name}"
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = "${module.network.self_link}"
}

###############################################
#
# Load balancer and VM for the pre bastion
#
###############################################

module "gcp-tcp-lb-bastion" {
  environment            = "${var.environment}"
  forwarding_port_ranges = "${var.tcp_lbs_bastion["forwarding_port_ranges"]}"
  fqdns                  = "${var.lb_fqdns_bastion}"
  gitlab_zone_id         = "${var.gitlab_com_zone_id}"
  health_check_ports     = "${var.tcp_lbs_bastion["health_check_ports"]}"
  instances              = ["${module.bastion.instances_self_link}"]
  lb_count               = "${length(var.tcp_lbs_bastion["names"])}"
  name                   = "gcp-tcp-lb-bastion"
  names                  = "${var.tcp_lbs_bastion["names"]}"
  project                = "${var.project}"
  region                 = "${var.region}"
  session_affinity       = "CLIENT_IP"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=remove-lifecycle"
  targets                = ["bastion"]
}

module "bastion" {
  bootstrap_version     = 6
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-bastion]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["bastion"]}"
  machine_type          = "${var.machine_types["bastion"]}"
  name                  = "bastion"
  node_count            = "${var.node_count["bastion"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["bastion"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.3"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Google storage buckets
#
##################################

module "storage" {
  environment                       = "${var.environment}"
  service_account_email             = "${var.service_account_email}"
  gcs_service_account_email         = "${var.gcs_service_account_email}"
  gcs_storage_analytics_group_email = "${var.gcs_storage_analytics_group_email}"
  source                            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets.git?ref=v1.2.0"
}

##################################
#
#  GKE Cluster for runners
#
##################################

# After provisioning you will need to configure
# the cluster for gitlab-runner. Instructions
# for this are in https://gitlab.com/gitlab-com/runbooks/tree/master/gke-runner

module "gke-runner" {
  environment           = "${var.environment}"
  name                  = "gke-runner"
  vpc                   = "${module.network.self_link}"
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/gke.git?ref=v1.0.3"
  initial_node_count    = 1
  ip_cidr_range         = "${var.subnetworks["gke-runner"]}"
  dns_zone_name         = "${var.dns_zone_name}"
  machine_type          = "${var.machine_types["gke-runner"]}"
  project               = "${var.project}"
  region                = "${var.region}"
  pod_ip_cidr_range     = "${var.subnetworks["gke-runner-pod-cidr"]}"
  service_ip_cidr_range = "${var.subnetworks["gke-runner-service-cidr"]}"
}

##################################
#
#  GKE Cluster for testing
#
##################################

# After provisioning you will need to configure

module "gke-pre" {
  environment           = "${var.environment}"
  name                  = "gke-pre"
  vpc                   = "${module.network.self_link}"
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/gke.git?ref=v1.0.3"
  initial_node_count    = 1
  ip_cidr_range         = "${var.subnetworks["gke-pre"]}"
  dns_zone_name         = "${var.dns_zone_name}"
  machine_type          = "${var.machine_types["gke-pre"]}"
  preemptible           = "true"
  project               = "${var.project}"
  region                = "${var.region}"
  pod_ip_cidr_range     = "${var.subnetworks["gke-pre-pod-cidr"]}"
  service_ip_cidr_range = "${var.subnetworks["gke-pre-service-cidr"]}"
}

##################################
#
#  External HAProxy LoadBalancer
#
##################################

module "fe-lb" {
  backend_service_type   = "regional"
  bootstrap_version      = "${var.bootstrap_script_version}"
  chef_provision         = "${var.chef_provision}"
  chef_run_list          = "\"role[${var.environment}-base-lb-fe]\""
  create_backend_service = false
  dns_zone_name          = "${var.dns_zone_name}"
  environment            = "${var.environment}"
  health_check           = "http"
  ip_cidr_range          = "${var.subnetworks["fe-lb"]}"
  machine_type           = "${var.machine_types["fe-lb"]}"
  name                   = "fe"
  node_count             = "${var.node_count["fe-lb"]}"
  os_boot_image          = "ubuntu-os-cloud/ubuntu-1804-bionic-v20190404"
  project                = "${var.project}"
  public_ports           = "${var.public_ports["fe-lb"]}"
  region                 = "${var.region}"
  service_account_email  = "${var.service_account_email}"
  service_path           = "/-/available-https"
  service_port           = 8002
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.3"
  tier                   = "lb"
  use_new_node_name      = true
  vpc                    = "${module.network.self_link}"
}

##################################
#
#  External HAProxy LoadBalancer Pages
#
##################################

module "fe-lb-pages" {
  bootstrap_version      = "${var.bootstrap_script_version}"
  chef_provision         = "${var.chef_provision}"
  chef_run_list          = "\"role[${var.environment}-base-lb-pages]\""
  create_backend_service = false
  dns_zone_name          = "${var.dns_zone_name}"
  environment            = "${var.environment}"
  health_check           = "http"
  ip_cidr_range          = "${var.subnetworks["fe-lb-pages"]}"
  machine_type           = "${var.machine_types["fe-lb"]}"
  name                   = "fe-pages"
  node_count             = "${var.node_count["fe-lb-pages"]}"
  project                = "${var.project}"
  public_ports           = "${var.public_ports["fe-lb"]}"
  region                 = "${var.region}"
  service_account_email  = "${var.service_account_email}"
  service_port           = 7331
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.3"
  tier                   = "lb"
  use_new_node_name      = true
  vpc                    = "${module.network.self_link}"
}

##################################
#
#  External HAProxy LoadBalancer Registry
#
##################################

module "fe-lb-registry" {
  bootstrap_version      = "${var.bootstrap_script_version}"
  chef_provision         = "${var.chef_provision}"
  chef_run_list          = "\"role[${var.environment}-base-lb-registry]\""
  create_backend_service = false
  dns_zone_name          = "${var.dns_zone_name}"
  environment            = "${var.environment}"
  health_check           = "http"
  ip_cidr_range          = "${var.subnetworks["fe-lb-registry"]}"
  machine_type           = "${var.machine_types["fe-lb"]}"
  name                   = "fe-registry"
  node_count             = "${var.node_count["fe-lb-registry"]}"
  project                = "${var.project}"
  public_ports           = "${var.public_ports["fe-lb"]}"
  region                 = "${var.region}"
  service_account_email  = "${var.service_account_email}"
  service_path           = "/-/available-https"
  service_port           = 8002
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.3"
  tier                   = "lb"
  use_new_node_name      = true
  vpc                    = "${module.network.self_link}"
}

##################################
#
#  GCP TCP LoadBalancers
#
##################################

#### Load balancer for the main site
module "gcp-tcp-lb" {
  environment            = "${var.environment}"
  forwarding_port_ranges = "${var.tcp_lbs["forwarding_port_ranges"]}"
  fqdns                  = "${var.lb_fqdns}"
  gitlab_zone_id         = "${var.gitlab_com_zone_id}"
  health_check_ports     = "${var.tcp_lbs["health_check_ports"]}"
  instances              = ["${module.fe-lb.instances_self_link}"]
  lb_count               = "${length(var.tcp_lbs["names"])}"
  name                   = "gcp-tcp-lb"
  names                  = "${var.tcp_lbs["names"]}"
  project                = "${var.project}"
  region                 = "${var.region}"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v1.0.0"
  targets                = ["fe"]
}

#### Load balancer for pages
module "gcp-tcp-lb-pages" {
  environment            = "${var.environment}"
  forwarding_port_ranges = "${var.tcp_lbs_pages["forwarding_port_ranges"]}"
  fqdns                  = "${var.lb_fqdns_pages}"
  gitlab_zone_id         = "${var.gitlab_io_zone_id}"
  health_check_ports     = "${var.tcp_lbs_pages["health_check_ports"]}"
  instances              = ["${module.fe-lb-pages.instances_self_link}"]
  lb_count               = "${length(var.tcp_lbs_pages["names"])}"
  name                   = "gcp-tcp-lb-pages"
  names                  = "${var.tcp_lbs_pages["names"]}"
  project                = "${var.project}"
  region                 = "${var.region}"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v1.0.0"
  targets                = ["fe-pages"]
}

#### Load balancer for registry
module "gcp-tcp-lb-registry" {
  environment            = "${var.environment}"
  forwarding_port_ranges = "${var.tcp_lbs_registry["forwarding_port_ranges"]}"
  fqdns                  = "${var.lb_fqdns_registry}"
  gitlab_zone_id         = "${var.gitlab_com_zone_id}"
  health_check_ports     = "${var.tcp_lbs_registry["health_check_ports"]}"
  instances              = ["${module.fe-lb-registry.instances_self_link}"]
  lb_count               = "${length(var.tcp_lbs_registry["names"])}"
  name                   = "gcp-tcp-lb-registry"
  names                  = "${var.tcp_lbs_registry["names"]}"
  project                = "${var.project}"
  region                 = "${var.region}"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v1.0.0"
  targets                = ["fe-registry"]
}

##################################
#
#  Deploy
#
##################################

module "deploy" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-deploy-node]\""
  dns_zone_name         = "${var.dns_zone_name}"
  egress_ports          = "${var.deploy_egress_ports}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["deploy"]}"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["deploy"]}"
  name                  = "deploy"
  node_count            = "${var.node_count["deploy"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["deploy"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.3"
  tier                  = "sv"
  use_external_ip       = true
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  registry front-end
#
#################################

module "registry" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-fe-registry]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["registry"]}"
  machine_type          = "${var.machine_types["registry"]}"
  name                  = "registry"
  node_count            = "${var.node_count["registry"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["registry"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.3"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Web front-end
#
#################################

module "web" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-fe-web]\""
  dns_zone_name         = "${var.dns_zone_name}"
  egress_ports          = "${var.egress_ports}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["web"]}"
  machine_type          = "${var.machine_types["web"]}"
  name                  = "web"
  node_count            = "${var.node_count["web"]}"
  os_disk_type          = "pd-ssd"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["web"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.3"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  API
#
#################################

module "api" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-fe-api]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["api"]}"
  machine_type          = "${var.machine_types["api"]}"
  name                  = "api"
  node_count            = "${var.node_count["api"]}"
  os_disk_type          = "pd-ssd"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["api"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.3"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Git
#
##################################

module "git" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-fe-git]\""
  dns_zone_name         = "${var.dns_zone_name}"
  egress_ports          = "${var.egress_ports}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["git"]}"
  machine_type          = "${var.machine_types["git"]}"
  name                  = "git"
  node_count            = "${var.node_count["git"]}"
  os_disk_type          = "pd-ssd"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["git"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.3"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Pages web front-end
#
#################################

module "web-pages" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-fe-web-pages]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["web-pages"]}"
  machine_type          = "${var.machine_types["web-pages"]}"
  name                  = "web-pages"
  node_count            = "${var.node_count["web-pages"]}"
  os_disk_type          = "pd-ssd"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["web-pages"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.3"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Sidekiq
#
##################################

module "sidekiq" {
  bootstrap_version                   = "${var.bootstrap_script_version}"
  chef_provision                      = "${var.chef_provision}"
  chef_run_list                       = "\"role[${var.environment}-base-be-sidekiq-besteffort]\""
  dns_zone_name                       = "${var.dns_zone_name}"
  environment                         = "${var.environment}"
  ip_cidr_range                       = "${var.subnetworks["sidekiq"]}"
  machine_type                        = "${var.machine_types["sidekiq-besteffort"]}"
  name                                = "sidekiq"
  os_disk_type                        = "pd-ssd"
  project                             = "${var.project}"
  public_ports                        = "${var.public_ports["sidekiq"]}"
  region                              = "${var.region}"
  service_account_email               = "${var.service_account_email}"
  sidekiq_asap_count                  = "0"
  sidekiq_asap_instance_type          = "none"
  sidekiq_besteffort_count            = "${var.node_count["sidekiq-besteffort"]}"
  sidekiq_besteffort_instance_type    = "${var.machine_types["sidekiq-besteffort"]}"
  sidekiq_elasticsearch_count         = "0"
  sidekiq_elasticsearch_instance_type = "none"
  sidekiq_import_count                = "0"
  sidekiq_import_instance_type        = "none"
  sidekiq_pages_count                 = "0"
  sidekiq_pages_instance_type         = "none"
  sidekiq_pipeline_count              = "0"
  sidekiq_pipeline_instance_type      = "none"
  sidekiq_pullmirror_count            = "0"
  sidekiq_pullmirror_instance_type    = "none"
  sidekiq_realtime_count              = "0"
  sidekiq_realtime_instance_type      = "none"
  sidekiq_traces_count                = "0"
  sidekiq_traces_instance_type        = "none"
  source                              = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-sidekiq.git?ref=v1.0.2"
  tier                                = "sv"
  use_new_node_name                   = true
  vpc                                 = "${module.network.self_link}"
}

##################################
#
#  Gitaly node
#
##################################

module "gitaly" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-be-gitaly]\""
  dns_zone_name         = "${var.dns_zone_name}"
  egress_ports          = "${var.egress_ports}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["gitaly"]}"
  machine_type          = "${var.machine_types["gitaly"]}"
  name                  = "gitaly"
  node_count            = "${var.node_count["gitaly"]}"
  os_disk_type          = "pd-ssd"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["gitaly"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.3"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}
