resource "azurerm_availability_set" "PagesLBStaging" {
  name                         = "PagesLBStaging"
  location                     = "${var.location}"
  managed                      = true
  platform_update_domain_count = 20
  platform_fault_domain_count  = 3
  resource_group_name          = "${var.resource_group_name}"
}

resource "azurerm_public_ip" "pages" {
  count                        = "${var.pages_lb_count}"
  name                         = "${format("pages%02d-stg-public-ip", count.index + 1)}"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "${format("pages%02d-stg", count.index + 1)}"
}

resource "azurerm_network_interface" "pages" {
  count                   = "${var.pages_lb_count}"
  name                    = "${format("pages%02d-stg", count.index + 1)}"
  internal_dns_name_label = "${format("pages%02d-stg", count.index + 1)}"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "${format("pages%02d-stg-default", count.index + 1)}"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.128.1.${format("%02d", count.index + 151)}"
    public_ip_address_id          = "${azurerm_public_ip.pages.*.id[count.index]}"
  }
}

resource "aws_route53_record" "pages" {
  count   = "${var.pages_lb_count}"
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "${format("pages%02d.stg.gitlab.com.", count.index + 1)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["${azurerm_public_ip.pages.*.fqdn[count.index]}."]
}

data "template_file" "chef-bootstrap-pages" {
  count    = "${var.pages_lb_count}"
  template = "${file("${path.root}/templates/chef-bootstrap.tpl")}"

  vars {
    ip_address          = "${azurerm_public_ip.pages.*.ip_address[count.index]}"
    hostname            = "${format("pages%02d.stg.gitlab.com", count.index + 1)}"
    chef_repo_dir       = "${var.chef_repo_dir}"
    first_user_username = "${var.first_user_username}"
    first_user_password = "${var.first_user_password}"
    chef_vaults         = "${var.chef_vaults}"
    chef_vault_env      = "${var.chef_vault_env}"
  }
}

resource "azurerm_virtual_machine" "pages" {
  count                         = "${var.pages_lb_count}"
  name                          = "${format("pages%02d.stg.gitlab.com", count.index + 1)}"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  availability_set_id           = "${azurerm_availability_set.PagesLBStaging.id}"
  network_interface_ids         = ["${azurerm_network_interface.pages.*.id[count.index]}"]
  primary_network_interface_id  = "${azurerm_network_interface.pages.*.id[count.index]}"
  vm_size                       = "Standard_A1_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${format("osdisk-pages%02d-stg", count.index + 1)}"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "${format("pages%02d.stg.gitlab.com", count.index + 1)}"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  provisioner "local-exec" {
    command = "${data.template_file.chef-bootstrap-pages.*.rendered[count.index]}"
  }

  provisioner "remote-exec" {
    inline = ["nohup bash -c 'sudo chef-client &'"]

    connection {
      type     = "ssh"
      host     = "${azurerm_public_ip.pages.*.ip_address[count.index]}"
      user     = "${var.first_user_username}"
      password = "${var.first_user_password}"
      timeout  = "10s"
    }
  }
}
