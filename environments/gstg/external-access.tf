##################################
#
#  gitlab-analysis
#
#################################

resource "google_compute_network_peering" "peering-gitlab-analysis" {
  name         = "peering-gitlab-analysis"
  network      = "${var.network_env}"
  peer_network = "https://www.googleapis.com/compute/v1/projects/gitlab-analysis/global/networks/default"
}

resource "google_compute_firewall" "allow-postgres-gitlab-analysis" {
  name        = "allow-postgres-gitlab-analysis"
  description = "allow gitlab-analysis default network to access gstg network"
  network     = "${var.network_env}"

  source_ranges = [
    "10.138.0.0/20", # only from us-west-1 default subnet
  ]

  target_tags = [
    "postgres-dr-archive",
  ]

  allow {
    protocol = "tcp"
    ports    = ["5432"]
  }
}
