##################################
#
#  DR
#
#################################

resource "google_compute_firewall" "allow-dr-postgres" {
  name        = "allow-dr-postgres"
  description = "Allows postgres traffic from our DR environment into gprd"
  network     = "${var.environment}"

  allow {
    protocol = "tcp"

    ports = [
      "5432",
    ]
  }

  source_ranges = [
    "10.251.9.0/24",
  ]

  target_tags = [
    "patroni",
  ]
}

resource "google_service_account" "dr-sa" {
  account_id = "disaster-recovery"
}

data "google_iam_policy" "dr-sa-access" {
  binding {
    role = "roles/storage.objectViewer"

    members = [
      "serviceAccount:${google_service_account.dr-sa.email}",
    ]
  }
}

##################################
#
#  gitlab-analysis
#
#################################

resource "google_compute_network_peering" "peering-gitlab-analysis" {
  name         = "peering-gitlab-analysis"
  network      = "${var.network_env}"
  peer_network = "https://www.googleapis.com/compute/v1/projects/gitlab-analysis/global/networks/default"
}

resource "google_compute_firewall" "allow-postgres-gitlab-analysis" {
  name        = "allow-postgres-gitlab-analysis"
  description = "allow gitlab-analysis default network to access gprd network"
  network     = "${var.network_env}"

  source_ranges = [
    "10.138.0.0/20", # only from us-west1 default subnet
  ]

  target_tags = [
    "postgres-dr-archive",
  ]

  allow {
    protocol = "tcp"
    ports    = ["5432"]
  }
}
