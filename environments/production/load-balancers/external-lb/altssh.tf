resource "azurerm_public_ip" "AltSSHLBProd" {
  name                         = "AltSSHLBProd"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
}

resource "azurerm_lb" "AltSSHLBProd" {
  name                = "AltSSHLBProd"
  location            = "${var.location}"
  resource_group_name = "${var.resource_group_name}"

  frontend_ip_configuration {
    name                 = "AltSSHLBProdIPConfiguration"
    public_ip_address_id = "${azurerm_public_ip.AltSSHLBProd.id}"
  }
}

resource "azurerm_lb_backend_address_pool" "AltSSHLBProd" {
  resource_group_name = "${var.resource_group_name}"
  loadbalancer_id     = "${azurerm_lb.AltSSHLBProd.id}"
  name                = "AltSSHLBProdPool"
}

resource "azurerm_lb_probe" "altssh" {
  resource_group_name = "${var.resource_group_name}"
  loadbalancer_id     = "${azurerm_lb.AltSSHLBProd.id}"
  name                = "altssh"
  port                = 443
  protocol            = "Tcp"
  interval_in_seconds = 5
  number_of_probes    = 2
}

resource "azurerm_lb_rule" "altssh" {
  resource_group_name            = "${var.resource_group_name}"
  loadbalancer_id                = "${azurerm_lb.AltSSHLBProd.id}"
  name                           = "altssh"
  protocol                       = "Tcp"
  frontend_ip_configuration_name = "AltSSHLBProdIPConfiguration"
  frontend_port                  = 443
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.AltSSHLBProd.id}"
  backend_port                   = 443
  load_distribution              = "SourceIPProtocol"
  probe_id                       = "${azurerm_lb_probe.altssh.id}"
}
