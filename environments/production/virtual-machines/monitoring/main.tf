variable "location" {
  description = "The location"
}

variable "resource_group_name" {
  description = "The name of the resource group"
}

variable "subnet_id" {}
variable "first_user_username" {}
variable "first_user_password" {}
variable "gitlab_net_zone_id" {}

resource "azurerm_availability_set" "MonitoringProd" {
  name                         = "MonitoringProd"
  location                     = "${var.location}"
  managed                      = true
  platform_update_domain_count = 1
  platform_fault_domain_count  = 1
  resource_group_name          = "${var.resource_group_name}"
}

output "availability_set_id" {
  value = "${azurerm_availability_set.MonitoringProd.id}"
}

resource "azurerm_public_ip" "performance-gitlab-net-public-ip" {
  name                         = "performance-gitlab-net-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "performance-prod"
}

resource "aws_route53_record" "performance-gitlab-net" {
  zone_id = "${var.gitlab_net_zone_id}"
  name    = "performance.gitlab.net"
  type    = "A"
  ttl     = "300"
  records = ["${azurerm_public_ip.performance-gitlab-net-public-ip.ip_address}"]
}

resource "azurerm_network_interface" "performance-gitlab-net-nic" {
  name                    = "performance-gitlab-net"
  internal_dns_name_label = "performance-gitlab-net"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "performance-gitlab-net-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.68.1.101"
    public_ip_address_id          = "${azurerm_public_ip.performance-gitlab-net-public-ip.id}"
  }
}

resource "azurerm_virtual_machine" "performance-gitlab-net" {
  name                          = "performance-gitlab-net"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  availability_set_id           = "${azurerm_availability_set.MonitoringProd.id}"
  network_interface_ids         = ["${azurerm_network_interface.performance-gitlab-net-nic.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-performance"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "performance.gitlab.net"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }
}
