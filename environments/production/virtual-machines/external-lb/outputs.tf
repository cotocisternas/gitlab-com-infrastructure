output "availability_set_id" {
  value = "${azurerm_availability_set.FrontEndLBProd.id}"
}

output "pages_availability_set_id" {
  value = "${azurerm_availability_set.PagesLBProd.id}"
}

output "altssh_availability_set_id" {
  value = "${azurerm_availability_set.AltSSHLBProd.id}"
}
