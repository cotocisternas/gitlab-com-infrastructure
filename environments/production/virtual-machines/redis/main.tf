variable "location" {}
variable "count" {}
variable "resource_group_name" {}
variable "subnet_id" {}
variable "first_user_username" {}
variable "first_user_password" {}
variable "chef_repo_dir" {}
variable "chef_vaults" {}

variable "chef_vault_env" {
  default = "_default"
}

variable "gitlab_com_zone_id" {}

resource "azurerm_availability_set" "RedisProd" {
  name                         = "RedisProd"
  location                     = "${var.location}"
  managed                      = true
  platform_update_domain_count = 20
  platform_fault_domain_count  = 3
  resource_group_name          = "${var.resource_group_name}"
}

output "availability_set_id" {
  value = "${azurerm_availability_set.RedisProd.id}"
}

resource "azurerm_public_ip" "redis" {
  count                        = "${var.count}"
  name                         = "${format("redis%02d-prod-public-ip", count.index + 1)}"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "${format("redis%02d-prod", count.index + 1)}"
}

resource "azurerm_network_interface" "redis" {
  count                   = "${var.count}"
  name                    = "${format("redis%02d-prod", count.index + 1)}"
  internal_dns_name_label = "${format("redis%02d-prod", count.index + 1)}"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "${format("redis%02d-prod", count.index + 1)}"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.66.2.${format("%02d", count.index + 101)}"
    public_ip_address_id          = "${azurerm_public_ip.redis.*.id[count.index]}"
  }
}

resource "aws_route53_record" "redis" {
  count   = "${var.count}"
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "${format("redis%02d.db.gitlab.com.", count.index + 1)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["${azurerm_public_ip.redis.*.fqdn[count.index]}."]
}

data "template_file" "chef-bootstrap-redis" {
  count    = "${var.count}"
  template = "${file("${path.root}/templates/chef-bootstrap.tpl")}"

  vars {
    ip_address          = "${azurerm_public_ip.redis.*.ip_address[count.index]}"
    hostname            = "${format("redis%02d.db.gitlab.com", count.index + 1)}"
    chef_repo_dir       = "${var.chef_repo_dir}"
    first_user_username = "${var.first_user_username}"
    first_user_password = "${var.first_user_password}"
    chef_vaults         = "${var.chef_vaults}"
    chef_vault_env      = "${var.chef_vault_env}"
  }
}

resource "azurerm_managed_disk" "redis-datadisk-0" {
  count                = "${var.count}"
  name                 = "${format("redis%02d-prod-datadisk-0", count.index + 1)}"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "128"
}

resource "azurerm_virtual_machine" "redis" {
  count                         = "${var.count}"
  name                          = "${format("redis%02d.db.gitlab.com", count.index + 1)}"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  availability_set_id           = "${azurerm_availability_set.RedisProd.id}"
  network_interface_ids         = ["${azurerm_network_interface.redis.*.id[count.index]}"]
  primary_network_interface_id  = "${azurerm_network_interface.redis.*.id[count.index]}"
  vm_size                       = "Standard_DS14_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${format("osdisk-redis%02d", count.index + 1)}"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.redis-datadisk-0.*.name[count.index]}"
    managed_disk_id = "${azurerm_managed_disk.redis-datadisk-0.*.id[count.index]}"
    disk_size_gb    = "${azurerm_managed_disk.redis-datadisk-0.*.disk_size_gb[count.index]}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  os_profile {
    computer_name  = "${format("redis%02d.db.gitlab.com", count.index + 1)}"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  provisioner "local-exec" {
    command = "${data.template_file.chef-bootstrap-redis.*.rendered[count.index]}"
  }

  provisioner "remote-exec" {
    inline = ["nohup bash -c 'sudo chef-client &'"]

    connection {
      type     = "ssh"
      host     = "${azurerm_public_ip.redis.*.ip_address[count.index]}"
      user     = "${var.first_user_username}"
      password = "${var.first_user_password}"
      timeout  = "10s"
    }
  }
}
