variable "project" {
  default = "gitlab-ops"
}

variable "region" {
  default = "us-east1"
}

variable "environment" {
  default = "ops"
}

variable "dns_zone_name" {
  default = "gitlab.net"
}

variable "default_kernel_version" {
  default = "4.15.0-1015"
}

variable "bootstrap_script_version" {
  default = 6
}

variable "oauth2_client_id_log_proxy" {}
variable "oauth2_client_secret_log_proxy" {}
variable "oauth2_client_id_dashboards" {}
variable "oauth2_client_secret_dashboards" {}
variable "oauth2_client_id_gitlab_ops" {}
variable "oauth2_client_secret_gitlab_ops" {}

variable "oauth2_client_id_monitoring" {}
variable "oauth2_client_secret_monitoring" {}

variable "machine_types" {
  type = "map"

  default = {
    "alerts"                = "n1-standard-1"
    "aptly"                 = "n1-standard-1"
    "log-proxy"             = "n1-standard-1"
    "proxy"                 = "n1-standard-1"
    "bastion"               = "n1-standard-1"
    "dashboards"            = "n1-standard-2"
    "dashboards-com"        = "n1-standard-4"
    "monitor"               = "n1-standard-8"
    "monitoring"            = "n1-standard-2"
    "gitlab-ops"            = "n1-standard-16"
    "runner-build"          = "n1-standard-32"
    "runner-chatops"        = "n1-standard-8"
    "runner-release"        = "n1-standard-8"
    "runner-release-single" = "n1-standard-1"
    "runner-snapshots"      = "n1-standard-1"
    "blackbox"              = "n1-standard-1"
    "sentry"                = "n1-standard-16"
    "sd-exporter"           = "n1-standard-1"
    "thanos-compact"        = "n1-standard-2"
    "thanos-query"          = "n1-standard-2"
    "thanos-store"          = "n1-highmem-8"
    "gke-runner"            = "n1-standard-2"
    "nessus"                = "n1-standard-4"
  }
}

variable "monitoring_hosts" {
  type = "map"

  default = {
    "names" = ["alerts", "prometheus", "prometheus-app", "thanos-query"]
    "ports" = [9093, 9090, 9090, 10902]
  }
}

variable "service_account_email" {
  type = "string"

  default = "terraform@gitlab-ops.iam.gserviceaccount.com"
}

# The ops network is allocated
# 10.250.0.0/16

variable "subnetworks" {
  type = "map"

  default = {
    "logging"          = "10.250.1.0/24"
    "bastion"          = "10.250.2.0/24"
    "dashboards"       = "10.250.3.0/24"
    "gitlab-ops"       = "10.250.4.0/24"
    "proxy"            = "10.250.5.0/24"
    "monitor"          = "10.250.6.0/24"
    "runner"           = "10.250.7.0/24"
    "monitoring"       = "10.250.8.0/24"
    "sentry"           = "10.250.9.0/24"
    "runner-chatops"   = "10.250.10.0/24"
    "dashboards-com"   = "10.250.11.0/24"
    "runner-release"   = "10.250.12.0/24"
    "gitlab-ops-geo"   = "10.250.13.0/24"
    "pubsubbeat"       = "10.250.14.0/24"
    "sd-exporter"      = "10.250.15.0/24"
    "gke-runner"       = "10.250.16.0/24"
    "runner-snapshots" = "10.250.17.0/24"
    "thanos-store"     = "10.250.18.0/24"
    "thanos-compact"   = "10.250.19.0/24"
    "aptly"            = "10.250.20.0/24"
  }
}

variable "public_ports" {
  type = "map"

  default = {
    "log-proxy"   = []
    "proxy"       = []
    "bastion"     = [22]
    "dashboards"  = []
    "gitlab-ops"  = [443, 80, 22, 5005]
    "pubsubbeat"  = []
    "runner"      = []
    "blackbox"    = []
    "sentry"      = [443, 80]
    "sd-exporter" = []
    "thanos"      = []
    "nessus"      = [8834]
    "aptly"       = [80]
  }
}

variable "node_count" {
  type = "map"

  default = {
    "alerts"         = 2
    "bastion"        = 1
    "blackbox"       = 1
    "dashboards"     = 1
    "gitlab-ops"     = 1
    "nessus"         = 1
    "prometheus"     = 1
    "prometheus-app" = 1
    "runner"         = 1
    "sentry"         = 1
    "sd-exporter"    = 1
    "thanos-compact" = 1
    "thanos-query"   = 1
    "thanos-store"   = 1
  }
}

variable "chef_provision" {
  type        = "map"
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-ops-chef-bootstrap"
    bootstrap_key     = "gitlab-ops-bootstrap-validation"
    bootstrap_keyring = "gitlab-ops-bootstrap"

    server_url    = "https://chef.gitlab.com/organizations/gitlab/"
    user_name     = "gitlab-ci"
    user_key_path = ".chef.pem"
    version       = "12.22.5"
  }
}

variable "monitoring_cert_link" {
  default = "projects/gitlab-ops/global/sslCertificates/wildcard-ops-gitlab-net"
}

variable "lb_fqdns_bastion" {
  type    = "list"
  default = ["lb-bastion.ops.gitlab.com"]
}

variable "network_testbed" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-testbed/global/networks/testbed"
}

variable "network_ops" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-ops/global/networks/ops"
}

variable "network_gprd" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-production/global/networks/gprd"
}

variable "network_gstg" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-staging-1/global/networks/gstg"
}

variable "network_dr" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-dr/global/networks/dr"
}

variable "network_pre" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-pre/global/networks/pre"
}

variable "tcp_lbs_bastion" {
  type = "map"

  default = {
    "names"                  = ["ssh"]
    "forwarding_port_ranges" = ["22"]
    "health_check_ports"     = ["80"]
  }
}

variable "tcp_lbs_sentry" {
  type = "map"

  default = {
    "names"                      = ["http", "https"]
    "forwarding_port_ranges"     = ["80", "443"]
    "health_check_ports"         = ["9000", "9000"]
    "health_check_request_paths" = ["/auth/login/gitlab/", "/auth/login/gitlab/"]
  }
}

variable "tcp_lbs_aptly" {
  type = "map"

  default = {
    "names"                  = ["http", "https"]
    "forwarding_port_ranges" = ["80", "443"]
    "health_check_ports"     = ["80", "80"]
  }
}

variable "log_gitlab_net_cert_link" {
  default = "projects/gitlab-ops/global/sslCertificates/log-gitlab-net"
}

variable "ops_gitlab_net_cert_link" {
  default = "projects/gitlab-ops/global/sslCertificates/ops-gitlab-net"
}

variable "dashboards_gitlab_net_cert_link" {
  default = "projects/gitlab-ops/global/sslCertificates/dashboards-gitlab-net"
}

variable "dashboards_gitlab_com_cert_link" {
  default = "projects/gitlab-ops/global/sslCertificates/dashboards-gitlab-com"
}

variable "gcs_service_account_email" {
  type    = "string"
  default = "gitlab-object-storage@gitlab-ops.iam.gserviceaccount.com"
}

# Service account used to do automated backup testing
# in https://gitlab.com/gitlab-restore/postgres-gprd

variable "gcs_postgres_backup_service_account" {
  type    = "string"
  default = "postgres-wal-archive@gitlab-ops.iam.gserviceaccount.com"
}

variable "gcs_postgres_restore_service_account" {
  type    = "string"
  default = "postgres-automated-backup-test@gitlab-restore.iam.gserviceaccount.com"
}

variable "gcs_postgres_backup_kms_key_id" {
  type    = "string"
  default = "projects/gitlab-ops/locations/global/keyRings/gitlab-secrets/cryptoKeys/ops-postgres-wal-archive"
}

variable "postgres_backup_retention_days" {
  type    = "string"
  default = "5"
}
