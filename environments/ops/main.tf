## State storage
terraform {
  backend "s3" {}
}

## AWS
provider "aws" {
  region = "us-east-1"
}

variable "gitlab_com_zone_id" {}
variable "gitlab_net_zone_id" {}

## Google

provider "google" {
  version = "~> 1.18.0"
  project = "${var.project}"
  region  = "${var.region}"
}

##################################
#
#  Network
#
#################################

module "network" {
  source      = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/vpc.git?ref=v1.0.0"
  project     = "${var.project}"
  environment = "${var.environment}"
}

##################################
#
#  Network Peering
#
#################################

resource "google_compute_network_peering" "peering_gprd" {
  name         = "peering-gprd"
  network      = "${var.network_ops}"
  peer_network = "${var.network_gprd}"
}

resource "google_compute_network_peering" "peering_gstg" {
  name         = "peering-gstg"
  network      = "${var.network_ops}"
  peer_network = "${var.network_gstg}"
}

resource "google_compute_network_peering" "peering_dr" {
  name         = "peering-dr"
  network      = "${var.network_ops}"
  peer_network = "${var.network_dr}"
}

resource "google_compute_network_peering" "peering_pre" {
  name         = "peering-pre"
  network      = "${var.network_ops}"
  peer_network = "${var.network_pre}"
}

resource "google_compute_network_peering" "peering_testbed" {
  name         = "peering-testbed"
  network      = "${var.network_ops}"
  peer_network = "${var.network_testbed}"
}

##################################
#
#  Log Proxy
#
#################################

module "proxy-iap" {
  environment          = "${var.environment}"
  source               = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/web-iap.git?ref=v1.0.0"
  name                 = "proxy"
  project              = "${var.project}"
  region               = "${var.region}"
  gitlab_zone_id       = "${var.gitlab_net_zone_id}"
  cert_link            = "${var.log_gitlab_net_cert_link}"
  backend_service_link = "${module.proxy.google_compute_backend_service_iap_self_link}"
  web_ip_fqdn          = "log.gitlab.net"
  service_ports        = ["443", "80", "9090"]
}

module "proxy" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-proxy]\""
  dns_zone_name         = "${var.dns_zone_name}"
  enable_iap            = true
  environment           = "${var.environment}"
  health_check          = "http"
  ip_cidr_range         = "${var.subnetworks["proxy"]}"
  machine_type          = "${var.machine_types["proxy"]}"
  name                  = "proxy"
  node_count            = 1
  oauth2_client_id      = "${var.oauth2_client_id_log_proxy}"
  oauth2_client_secret  = "${var.oauth2_client_secret_log_proxy}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["proxy"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = "9090"
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Pubsubbeats
#
#  Machines for running the beats
#  that consume logs from pubsub
#  and send them to elastic cloud
#
#  You must have a chef role with the
#  following format:
#     role[<env>-infra-pubsubbeat-<beat_name>]
#
##################################

module "pubsubbeat" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["pubsubbeat"]}"
  machine_types         = "${var.pubsubbeats["machine_types"]}"
  names                 = "${var.pubsubbeats["names"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["pubsubbeat"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/pubsubbeat.git?ref=v1.0.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Monitoring
#
#  Uses the monitoring module, this
#  creates a single instance behind
#  a load balancer with identity aware
#  proxy enabled.
#
##################################

resource "google_compute_subnetwork" "monitoring" {
  ip_cidr_range            = "${var.subnetworks["monitoring"]}"
  name                     = "${format("monitoring-%v", var.environment)}"
  network                  = "${module.network.self_link}"
  private_ip_google_access = true
  project                  = "${var.project}"
  region                   = "${var.region}"
}

#######################
#
# load balancer for all hosts in this section
#
#######################

module "monitoring-lb" {
  cert_link          = "${var.monitoring_cert_link}"
  environment        = "${var.environment}"
  gitlab_net_zone_id = "${var.gitlab_net_zone_id}"
  hosts              = ["${var.monitoring_hosts["names"]}"]
  name               = "monitoring-lb"
  project            = "${var.project}"
  region             = "${var.region}"
  service_ports      = ["${var.monitoring_hosts["ports"]}"]
  source             = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-lb.git?ref=v1.0.0"
  subnetwork_name    = "${google_compute_subnetwork.monitoring.name}"
  targets            = ["${var.monitoring_hosts["names"]}"]
  url_map            = "${google_compute_url_map.monitoring-lb.self_link}"
}

#######################
module "prometheus" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-prometheus]\""
  data_disk_size        = 50
  data_disk_type        = "pd-ssd"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  machine_type          = "${var.machine_types["monitoring"]}"
  name                  = "prometheus"
  node_count            = "${var.node_count["prometheus"]}"
  oauth2_client_id      = "${var.oauth2_client_id_monitoring}"
  oauth2_client_secret  = "${var.oauth2_client_secret_monitoring}"
  persistent_disk_path  = "/opt/prometheus"
  project               = "${var.project}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_path          = "/graph"
  service_port          = "${element(var.monitoring_hosts["ports"], index(var.monitoring_hosts["names"], "prometheus"))}"
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v1.0.3"
  subnetwork_name       = "${google_compute_subnetwork.monitoring.name}"
  tier                  = "inf"
  use_external_ip       = true
  use_new_node_name     = true
}

module "prometheus-app" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-prometheus-app]\""
  data_disk_size        = 50
  data_disk_type        = "pd-ssd"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  machine_type          = "${var.machine_types["monitoring"]}"
  name                  = "prometheus-app"
  node_count            = "${var.node_count["prometheus-app"]}"
  oauth2_client_id      = "${var.oauth2_client_id_monitoring}"
  oauth2_client_secret  = "${var.oauth2_client_secret_monitoring}"
  persistent_disk_path  = "/opt/prometheus"
  project               = "${var.project}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_path          = "/graph"
  service_port          = "${element(var.monitoring_hosts["ports"], index(var.monitoring_hosts["names"], "prometheus-app"))}"
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v1.0.3"
  subnetwork_name       = "${google_compute_subnetwork.monitoring.name}"
  tier                  = "inf"
  use_external_ip       = true
  use_new_node_name     = true
}

module "alerts" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-alerts]\""
  data_disk_size        = 100
  data_disk_type        = "pd-standard"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  machine_type          = "${var.machine_types["alerts"]}"
  name                  = "alerts"
  node_count            = "${var.node_count["alerts"]}"
  oauth2_client_id      = "${var.oauth2_client_id_monitoring}"
  oauth2_client_secret  = "${var.oauth2_client_secret_monitoring}"
  persistent_disk_path  = "/opt"
  project               = "${var.project}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = "${element(var.monitoring_hosts["ports"], index(var.monitoring_hosts["names"], "alerts"))}"
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v1.0.1"
  subnetwork_name       = "${google_compute_subnetwork.monitoring.name}"
  tier                  = "inf"
  use_new_node_name     = true
}

module "thanos-query" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-thanos-query]\""
  data_disk_size        = 100
  data_disk_type        = "pd-standard"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  machine_type          = "${var.machine_types["thanos-query"]}"
  name                  = "thanos-query"
  node_count            = "${var.node_count["thanos-query"]}"
  oauth2_client_id      = "${var.oauth2_client_id_monitoring}"
  oauth2_client_secret  = "${var.oauth2_client_secret_monitoring}"
  persistent_disk_path  = "/opt"
  project               = "${var.project}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = "${element(var.monitoring_hosts["ports"], index(var.monitoring_hosts["names"], "thanos-query"))}"
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v1.0.1"
  subnetwork_name       = "${google_compute_subnetwork.monitoring.name}"
  tier                  = "inf"
  use_new_node_name     = true
}

module "sd-exporter" {
  additional_scopes         = ["https://www.googleapis.com/auth/monitoring"]
  allow_stopping_for_update = true
  bootstrap_version         = 6
  chef_provision            = "${var.chef_provision}"
  chef_run_list             = "\"role[${var.environment}-infra-sd-exporter]\""
  create_backend_service    = false
  dns_zone_name             = "${var.dns_zone_name}"
  environment               = "${var.environment}"
  machine_type              = "${var.machine_types["sd-exporter"]}"
  name                      = "sd-exporter"
  node_count                = "${var.node_count["sd-exporter"]}"
  project                   = "${var.project}"
  public_ports              = "${var.public_ports["sd-exporter"]}"
  region                    = "${var.region}"
  service_account_email     = "${var.service_account_email}"
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.1"
  subnetwork_name           = "${google_compute_subnetwork.monitoring.name}"
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = "${module.network.self_link}"
}

module "blackbox" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-blackbox]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["blackbox"]}"
  name                  = "blackbox"
  node_count            = "${var.node_count["blackbox"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["blackbox"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.1"
  subnetwork_name       = "${google_compute_subnetwork.monitoring.name}"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
  use_external_ip       = true
}

module "thanos-store" {
  bootstrap_version     = "6"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-thanos-store]\""
  data_disk_size        = 100
  data_disk_type        = "pd-ssd"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  ip_cidr_range         = "${var.subnetworks["thanos-store"]}"
  machine_type          = "${var.machine_types["thanos-store"]}"
  name                  = "thanos-store"
  node_count            = "${var.node_count["thanos-store"]}"
  persistent_disk_path  = "/opt/prometheus"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["thanos"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v1.0.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

module "thanos-compact" {
  bootstrap_version     = "6"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-thanos-compact]\""
  data_disk_size        = 100
  data_disk_type        = "pd-ssd"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  ip_cidr_range         = "${var.subnetworks["thanos-compact"]}"
  machine_type          = "${var.machine_types["thanos-compact"]}"
  name                  = "thanos-compact"
  node_count            = "${var.node_count["thanos-compact"]}"
  persistent_disk_path  = "/opt/prometheus"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["thanos"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v1.0.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

#######################################################
#
# Tenable.IO local Nessus scanner
#
#######################################################

module "nessus" {
  bootstrap_version     = 8
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-nessus]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  kernel_version        = "${var.default_kernel_version}"
  machine_type          = "${var.machine_types["nessus"]}"
  name                  = "nessus"
  node_count            = "${var.node_count["nessus"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["nessus"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.2"
  subnetwork_name       = "${google_compute_subnetwork.monitoring.name}"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
  use_external_ip       = true
}

#######################################################
#
# Load balancer and VM for dashboards.gitlab.net
#
#######################################################

module "dashboards-internal" {
  environment          = "${var.environment}"
  source               = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/web-iap.git?ref=v1.0.0"
  name                 = "dashboards"
  project              = "${var.project}"
  region               = "${var.region}"
  create_http_forward  = "true"
  gitlab_zone_id       = "${var.gitlab_net_zone_id}"
  cert_link            = "${var.dashboards_gitlab_net_cert_link}"
  backend_service_link = "${module.dashboards.google_compute_backend_service_noiap_self_link}"
  web_ip_fqdn          = "dashboards.gitlab.net"
  service_ports        = ["80", "443", "3000"]
}

module "dashboards" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-dashboards]\""
  data_disk_size        = 100
  data_disk_type        = "pd-ssd"
  dns_zone_name         = "${var.dns_zone_name}"
  enable_iap            = false
  environment           = "${var.environment}"
  health_check          = "http"
  health_check_port     = 3000
  ip_cidr_range         = "${var.subnetworks["dashboards"]}"
  machine_type          = "${var.machine_types["dashboards"]}"
  name                  = "dashboards"
  node_count            = 1
  persistent_disk_path  = "/var/lib/grafana"
  project               = "${var.project}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_path          = "/api/health"
  service_port          = 80
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v1.0.1"
  tier                  = "inf"
  use_external_ip       = true
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

#######################################################
#
# Load balancer and VM for dashboards.gitlab.com
#
#######################################################

module "dashboards-com-lb" {
  environment          = "${var.environment}"
  source               = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/web-iap.git?ref=v1.0.0"
  name                 = "dashboards-com"
  project              = "${var.project}"
  region               = "${var.region}"
  create_http_forward  = "true"
  gitlab_zone_id       = "${var.gitlab_com_zone_id}"
  cert_link            = "${var.dashboards_gitlab_com_cert_link}"
  backend_service_link = "${module.dashboards-com.google_compute_backend_service_noiap_self_link}"
  web_ip_fqdn          = "dashboards.gitlab.com"
  service_ports        = ["80", "443", "3000"]
}

module "dashboards-com" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-public-dashboards]\""
  data_disk_size        = 100
  data_disk_type        = "pd-standard"
  dns_zone_name         = "${var.dns_zone_name}"
  enable_iap            = false
  environment           = "${var.environment}"
  health_check          = "http"
  health_check_port     = 3000
  ip_cidr_range         = "${var.subnetworks["dashboards-com"]}"
  machine_type          = "${var.machine_types["dashboards-com"]}"
  name                  = "dashboards-com"
  node_count            = 1
  persistent_disk_path  = "/var/lib/grafana"
  project               = "${var.project}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_path          = "/api/health"
  service_port          = 80
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v1.0.1"
  tier                  = "inf"
  use_external_ip       = true
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

#######################################################
#
# VM for ops.gitlab.net
#
#######################################################

resource "aws_route53_record" "default" {
  zone_id = "${var.gitlab_net_zone_id}"
  name    = "ops.gitlab.net"
  type    = "A"
  ttl     = "300"
  records = ["${module.gitlab-ops.instance_public_ips[0]}"]
}

resource "aws_route53_record" "ops-registry" {
  zone_id = "${var.gitlab_net_zone_id}"
  name    = "registry.ops.gitlab.net"
  type    = "A"
  ttl     = "300"
  records = ["${module.gitlab-ops.instance_public_ips[0]}"]
}

module "gitlab-ops" {
  backend_protocol      = "HTTPS"
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-gitlab-primary]\""
  data_disk_size        = 5000
  data_disk_type        = "pd-ssd"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "http"
  health_check_port     = 8887
  ip_cidr_range         = "${var.subnetworks["gitlab-ops"]}"
  machine_type          = "${var.machine_types["gitlab-ops"]}"
  name                  = "gitlab"
  node_count            = 1
  oauth2_client_id      = "${var.oauth2_client_id_gitlab_ops}"
  oauth2_client_secret  = "${var.oauth2_client_secret_gitlab_ops}"
  persistent_disk_path  = "/var/opt/gitlab"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["gitlab-ops"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_path          = "/-/liveness"
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v1.0.1"
  tier                  = "inf"
  use_external_ip       = true
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

#######################################################
#
# VM for ops-geo.gitlab.net
#
#######################################################

resource "aws_route53_record" "gitlab-ops-geo" {
  zone_id = "${var.gitlab_net_zone_id}"
  name    = "geo.ops.gitlab.net"
  type    = "A"
  ttl     = "300"
  records = ["${module.gitlab-ops-geo.instance_public_ips[0]}"]
}

module "gitlab-ops-geo" {
  backend_protocol      = "HTTPS"
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-gitlab-secondary]\""
  data_disk_size        = 5000
  data_disk_type        = "pd-ssd"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "http"
  health_check_port     = 8887
  ip_cidr_range         = "${var.subnetworks["gitlab-ops-geo"]}"
  machine_type          = "${var.machine_types["gitlab-ops"]}"
  name                  = "gitlab-geo"
  node_count            = 1
  oauth2_client_id      = "${var.oauth2_client_id_gitlab_ops}"
  oauth2_client_secret  = "${var.oauth2_client_secret_gitlab_ops}"
  persistent_disk_path  = "/var/opt/gitlab"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["gitlab-ops"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_path          = "/-/liveness"
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v1.0.1"
  tier                  = "inf"
  use_external_ip       = true
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

###############################################
#
# Load balancer and VM for the ops bastion
#
###############################################

module "gcp-tcp-lb-bastion" {
  environment            = "${var.environment}"
  forwarding_port_ranges = "${var.tcp_lbs_bastion["forwarding_port_ranges"]}"
  fqdns                  = "${var.lb_fqdns_bastion}"
  gitlab_zone_id         = "${var.gitlab_com_zone_id}"
  health_check_ports     = "${var.tcp_lbs_bastion["health_check_ports"]}"
  instances              = ["${module.bastion.instances_self_link}"]
  lb_count               = "${length(var.tcp_lbs_bastion["names"])}"
  name                   = "gcp-tcp-lb-bastion"
  names                  = "${var.tcp_lbs_bastion["names"]}"
  project                = "${var.project}"
  region                 = "${var.region}"
  session_affinity       = "CLIENT_IP"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v1.0.0"
  targets                = ["bastion"]
}

module "bastion" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-bastion]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["bastion"]}"
  machine_type          = "${var.machine_types["bastion"]}"
  name                  = "bastion"
  node_count            = "${var.node_count["bastion"]}"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["bastion"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Runner
#
##################################

module "runner" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-runner-build]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["runner"]}"
  machine_type          = "${var.machine_types["runner-build"]}"
  name                  = "runner"
  node_count            = "${var.node_count["runner"]}"
  os_disk_size          = 100
  project               = "${var.project}"
  public_ports          = "${var.public_ports["runner"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Runner for ChatOps
#
##################################

module "runner-chatops" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-runner-chatops]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["runner-chatops"]}"
  machine_type          = "${var.machine_types["runner-chatops"]}"
  name                  = "runner-chatops"
  node_count            = "${var.node_count["runner"]}"
  os_disk_size          = 100
  project               = "${var.project}"
  public_ports          = "${var.public_ports["runner"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Runner for Release
#
##################################

module "runner-release" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-runner-release]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["runner-release"]}"
  machine_type          = "${var.machine_types["runner-release"]}"
  name                  = "runner-release"
  node_count            = "${var.node_count["runner"]}"
  os_disk_size          = 100
  project               = "${var.project}"
  public_ports          = "${var.public_ports["runner"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

# This is a special release runner with a concurrency of 1 so
# that we can enforce single pipeline execution. This prevents
# multiple release pipelines from deploying simultaneously.
#
# This runner is used for the first job and the regular release
# release number (with high concurrency) is used for the later
# stages where there are many jobs in parallel.

module "runner-release-single" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-runner-release-single]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["runner-release"]}"
  machine_type          = "${var.machine_types["runner-release-single"]}"
  name                  = "runner-release-single"
  node_count            = "${var.node_count["runner"]}"
  os_disk_size          = 100
  project               = "${var.project}"
  public_ports          = "${var.public_ports["runner"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.1"
  subnetwork_name       = "${module.runner-release.google_compute_subnetwork_name}"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Runner for drive snapshot creation and restoring
#
##################################

module "runner-snapshots" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-runner-snapshots]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["runner-snapshots"]}"
  machine_type          = "${var.machine_types["runner-snapshots"]}"
  name                  = "runner-snapshots"
  node_count            = "${var.node_count["runner"]}"
  os_disk_size          = 100
  project               = "${var.project}"
  public_ports          = "${var.public_ports["runner"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.1"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Sentry
#
##################################

module "sentry-lb" {
  environment                = "${var.environment}"
  forwarding_port_ranges     = "${var.tcp_lbs_sentry["forwarding_port_ranges"]}"
  fqdns                      = ["sentry.gitlab.net"]
  gitlab_zone_id             = "${var.gitlab_net_zone_id}"
  health_check_ports         = "${var.tcp_lbs_sentry["health_check_ports"]}"
  health_check_request_paths = "${var.tcp_lbs_sentry["health_check_request_paths"]}"
  instances                  = ["${module.sentry.instances_self_link}"]
  lb_count                   = "${length(var.tcp_lbs_sentry["names"])}"
  name                       = "ops-gcp-tcp-lb-sentry"
  names                      = "${var.tcp_lbs_sentry["names"]}"
  project                    = "${var.project}"
  region                     = "${var.region}"
  source                     = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v1.0.0"
  targets                    = ["sentry"]
}

module "sentry" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-infra-sentry]\""
  data_disk_size        = 2000
  data_disk_type        = "pd-ssd"
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  ip_cidr_range         = "${var.subnetworks["sentry"]}"
  machine_type          = "${var.machine_types["sentry"]}"
  name                  = "sentry"
  node_count            = "${var.node_count["sentry"]}"
  os_disk_size          = 100
  persistent_disk_path  = "/var/lib/postgresql"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["sentry"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v1.0.1"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Aptly
#
##################################

module "aptly-lb" {
  environment            = "${var.environment}"
  forwarding_port_ranges = "${var.tcp_lbs_aptly["forwarding_port_ranges"]}"
  fqdns                  = ["aptly.gitlab.com"]
  gitlab_zone_id         = "${var.gitlab_net_zone_id}"
  health_check_ports     = "${var.tcp_lbs_aptly["health_check_ports"]}"
  instances              = ["${module.aptly.instances_self_link}"]
  lb_count               = "${length(var.tcp_lbs_aptly["names"])}"
  name                   = "ops-gcp-tcp-lb-aptly"
  names                  = "${var.tcp_lbs_aptly["names"]}"
  project                = "${var.project}"
  region                 = "${var.region}"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v1.0.0"
  targets                = ["aptly"]
}

module "aptly" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[aptly-gitlab-com]\""
  dns_zone_name         = "${var.dns_zone_name}"
  environment           = "${var.environment}"
  ip_cidr_range         = "${var.subnetworks["aptly"]}"
  machine_type          = "${var.machine_types["aptly"]}"
  name                  = "aptly"
  node_count            = 1
  persistent_disk_path  = "/opt"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["aptly"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v1.0.1"
  tier                  = "sv"
  use_external_ip       = true
  use_new_node_name     = true
  vpc                   = "${module.network.self_link}"
}

##################################
#
#  Google storage buckets
#
##################################

module "storage" {
  environment                       = "${var.environment}"
  versioning                        = "${var.versioning}"
  artifact_age                      = "${var.artifact_age}"
  lfs_object_age                    = "${var.lfs_object_age}"
  package_repo_age                  = "${var.package_repo_age}"
  upload_age                        = "${var.upload_age}"
  storage_log_age                   = "${var.storage_log_age}"
  storage_class                     = "${var.storage_class}"
  service_account_email             = "${var.service_account_email}"
  gcs_service_account_email         = "${var.gcs_service_account_email}"
  gcs_storage_analytics_group_email = "${var.gcs_storage_analytics_group_email}"
  source                            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets.git?ref=v1.0.0"
}

##################################
#
#  GitLab Billing bucket
#
##################################

resource "google_storage_bucket" "gitlab-billing" {
  name = "gitlab-billing"

  versioning = {
    enabled = "false"
  }

  storage_class = "NEARLINE"

  labels = {
    tfmanaged = "yes"
  }
}

resource "google_storage_bucket_iam_binding" "billing-viewer-binding" {
  bucket = "gitlab-billing"
  role   = "roles/storage.objectViewer"

  members = [
    "serviceAccount:import@gitlab-analysis.iam.gserviceaccount.com",
  ]
}

resource "google_storage_bucket_iam_binding" "billing-legacy-bucket-binding" {
  bucket = "gitlab-billing"
  role   = "roles/storage.legacyBucketReader"

  members = [
    "serviceAccount:import@gitlab-analysis.iam.gserviceaccount.com",
    "projectViewer:gitlab-ops",
  ]
}

resource "google_storage_bucket_iam_binding" "billing-legacy-object-binding" {
  bucket = "gitlab-billing"
  role   = "roles/storage.legacyObjectReader"

  members = [
    "serviceAccount:import@gitlab-analysis.iam.gserviceaccount.com",
  ]
}

##################################
#
#  GKE Cluster for runners
#
##################################

# After provisioning you will need to configure
# the cluster for gitlab-runner. Instructions
# for this are in https://gitlab.com/gitlab-com/runbooks/tree/master/gke-runner

module "gke-runner" {
  environment        = "${var.environment}"
  name               = "gke-runner"
  vpc                = "${module.network.self_link}"
  source             = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/gke.git?ref=v1.0.0"
  initial_node_count = 1
  ip_cidr_range      = "${var.subnetworks["gke-runner"]}"
  dns_zone_name      = "${var.dns_zone_name}"
  machine_type       = "${var.machine_types["gke-runner"]}"
  project            = "${var.project}"
  region             = "${var.region}"
}

##################################
#
#  GCS Bucket for postgres backup
#
##################################

module "postgres-backup" {
  environment                         = "${var.environment}"
  gcs_postgres_backup_service_account = "${var.gcs_postgres_backup_service_account}"
  restore_service_account             = "${var.gcs_postgres_restore_service_account}"
  kms_key_id                          = "${var.gcs_postgres_backup_kms_key_id}"
  source                              = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/database-backup-bucket.git?ref=v1.0.0"
  retention_days                      = "${var.postgres_backup_retention_days}"
}

##################################
#
#  Service accounts
#
##################################

## Service account used for granting ops
## write access to asset buckets

resource "google_service_account" "assets" {
  account_id   = "asset-uploader"
  display_name = "Service account that allows ops to write to assets buckets in other projects"
}
