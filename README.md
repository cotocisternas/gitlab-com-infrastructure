# GitLab.com Infrastructure

## What is this?

Here you can find the Terraform configuration for GitLab.com, the staging environment and possibly something else.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written. Reading [Terraform: Up and Running](https://www.amazon.com/Terraform-Running-Writing-Infrastructure-Code-ebook/dp/B06XKHGJHP) is also recommended.

## How the repository is structured

Each environment has its own directory.

There is currently one [Terraform state](https://www.terraform.io/docs/state/index.html) for each environment although we're considering to split at least the production one into smaller ones.

Cattle is handled in loops while pets require the full configuration to be
coded. This means you only need to change `count` in `main.tf` to scale a fleet
in and out. More information about [managing cattle can be found in our
docs](./docs/working-with-cattle.md)

### Modules

While some modules are still maintained under the `./modules` directory, we are actively migrating
module code to standalone repositories under the
[`gitlab-com/gl-infra/terraform-modules`](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules) group in
the ops.gitlab.net instance (see [infrastructure#5688](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/5688) for
current progress).

## Setup (How can I use this?)

First and foremost, remember that Terraform is an extremely powerful tool so be sure you know how to use it well before applying changes in production. Always `plan` and don't hesitate to ask in `#production` if you are in doubt or you see something off in the plan, even the smallest thing.

* [Create a new staging environment](https://drive.google.com/open?id=0BzamLYNnSQa_cjN5NGtaRnpyRXc) (video - internal only)<br>Watch this for a complete overview of how to work with Terraform.

### Making changes

We value small changes.  There are times when we need to make changes across a large number of hosts in the infrastructure.  If possible, break your changes down into smallest subset of functional changes in order to limit the effects of a change at first.  This also helps us keep the master branch `terraform apply`-able.  We have learned from past incidents that it can be hard to untangle big changes.  We may have multiple people working on terraform at the same time so keeping your changes smaller, concise, and more incremental will make all changes easier to incorporate.  

We have a badge at the top - before you make a branch off of master, you will want to make sure that TF plan is clean on the environment you plan to change.  This will hopefully help keep you from getting into a situation where the state file gets complicated.  

### Important notes

* We use differing versions of terraform in our environments.  You'll want to
  ensure you have all available versions prior running anything in terraform.
  * `find ./ -name .terraform-version -exec cat {} \;`
  * our wrapper scripts mentioned above will help protect you from running
    terraform with the incorrect desired version
* Make sure you're not specifying `knife[:identity_file]` in your `.chef/knife.rb`. If you do need it then comment it out every time you create a new node. If you don't then the node provisioning will fail with an error like this: `Errno::ENODEV: Operation not supported by device`.
* Be extremely careful when you initialize the state for the first time. Do not load the variables on staging and then `cd` to production to initialize the state: this would link production to the staging state and you do not want that. It's good practice to initialize the states as soon as you set up your environment to avoid making mistakes, so:
```
cd staging
tf-init
cd ../production
tf-init
...
```
Then start working.

#### Getting Started:

  1. Clone [chef-repo](https://dev.gitlab.org/cookbooks/chef-repo) if you haven't already.
  1. Install `pwgen`. If you're using brew you can simply run `brew install pwgen`.
  1. It is highly suggested to utilize some sort of terraform versioning manager such as [tfenv](https://github.com/kamatama41/tfenv) with `brew install tfenv` in order to manage multiple versions of Terraform. So if _for instance_ you want to install version 0.9.8 all you have to do it run `tfenv install 0.9.8` and enable it with `tfenv use 0.9.8`. To verify this, run `terraform -version` and check the version string. See the _Important Notes_ section above to determine which versions are used in our environment.
  1. There are two wrapper scripts in the `bin/` directory that should be used in place of the `terraform` executable, `tf` and `tf-init`. Ensure that `gitlab-com-infrastructure/bin` is in your $PATH
  1. You need to provide some secrets to Terraform using shell env vars. Wrappers in `bin/` source a number of files in `private/env_vars`. The contents of these files can be found in 1Password vault called Production. In 1password search for "terraform-private" and create each of the `*.env` files in the `private/env_vars` directory. For example:
```
./private/env_vars/common.env
./private/env_vars/gprd.env
```
  1. Be sure that the [gcloud-cli is configured and authenticated](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/gcloud-cli.md)
  1. Set up access via [GSTG bastion hosts](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/gstg-bastions.md)
  1. See Important notes section above about initializing terraform. Run `tf-init` and `tf plan` in the `environments/gstg` directory and ensure that terraform runs and you can see pending changes

### Caveats

- Don't use `tf destroy` on cattle unless you **really** know what you're doing. Be aware that deleting an element of an array other than the last one will result in all the resources starting from that element to be destroyed as well. So if you delete `module.something.resource[0]` Terraform will delete **everything** on that array, even if the confirmation dialog only reports the one you want to delete.
- Requesting a quota increase on GCP generally takes an hour or two for memory and CPU increases, but takes several days for increases in SSD and standard disk space.

---

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
