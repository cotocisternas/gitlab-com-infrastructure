#################
# Monitoring whitelist
#################

#################
# Allow traffic from the ops
# network from grafana
#################

variable "monitoring_whitelist_prometheus" {
  type = "map"

  default = {
    # 10.250.3.x for the internal grafana
    # 10.250.11.x for the public grafana
    # 10.250.8.x for the ops prometheus servers
    #
    # Port 9090 is for prometheus
    # Ports 10900-10902 is for thanos
    "subnets" = ["10.250.3.0/24", "10.250.11.0/24", "10.250.8.0/24"]

    "ports" = ["9090", "10900", "10901", "10902"]
  }
}

variable "monitoring_whitelist_influxdb" {
  type = "map"

  default = {
    # 10.250.3.x for internal dashboards
    "subnets" = ["10.250.3.0/24"]
    "ports"   = ["8086"]
  }
}

variable "monitoring_whitelist_thanos" {
  type = "map"

  default = {
    # 10.250.8.x for the ops prometheus servers
    # 10.250.3.x for the internal grafana
    # 10.250.11.x for the public grafana
    "subnets" = ["10.250.3.0/24", "10.250.11.0/24", "10.250.8.0/24"]

    "ports" = ["10901", "10902"]
  }
}

#################
# Allow traffic from the ops
# network from the alerts manager
#################
variable "monitoring_whitelist_alerts" {
  type = "map"

  default = {
    # 10.250.8.x for the ops alerts servers
    "subnets" = ["10.250.8.0/24"]
    "ports"   = ["9093"]
  }
}

####################################
# Default log filters for stackdriver
#####################################

variable "sd_log_filters" {
  type = "map"

  default = {
    "exclude_logtypes" = "resource.type=\"gce_instance\" AND (labels.tag:\"workhorse\" OR labels.tag:\"rails\" OR labels.tag:\"workhorse\" OR labels.tag:\"gitaly\")"
  }
}
