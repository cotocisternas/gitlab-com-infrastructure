variable "location" {}
variable "environment" {}
variable "name" {}
variable "ssh_key_path" {}
variable "ssh_user" {}
variable "resource_group_name" {}
variable "backup_aws_access_key" {}
variable "backup_aws_secret_key" {}
variable "security_group_id" {}
variable "enable_module" {}
variable "zone_id" {}

variable "disk_names" {
  default = []
}

variable "disk_ids" {
  default = []
}

variable "disk_sizes" {
  default = []
}

resource "null_resource" "pre_keypair" {
  count = "${var.enable_module ? 1 : 0}"

  provisioner "local-exec" {
    command = "${path.root}/../../bin/create-ssh-key ${var.ssh_user}-${var.environment}-${terraform.workspace}"
  }
}

resource "azurerm_public_ip" "single" {
  count                        = "${var.enable_module ? 1 : 0}"
  name                         = "public-ip-${var.name}"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
}

resource "azurerm_virtual_network" "single" {
  count               = "${var.enable_module ? 1 : 0}"
  name                = "virtual-network-${var.name}"
  address_space       = ["10.0.0.0/8"]
  location            = "${var.location}"
  resource_group_name = "${var.resource_group_name}"
}

resource "azurerm_resource_group" "single" {
  count    = "${var.enable_module ? 1 : 0}"
  name     = "${var.environment}"
  location = "${var.location}"
}

resource "azurerm_subnet" "single" {
  count                = "${var.enable_module ? 1 : 0}"
  name                 = "${var.name}"
  resource_group_name  = "${var.resource_group_name}"
  virtual_network_name = "${azurerm_virtual_network.single.name}"
  address_prefix       = "10.0.2.0/24"
}

resource "azurerm_network_interface" "single" {
  count                     = "${var.enable_module ? 1 : 0}"
  name                      = "network-interface-${var.name}"
  location                  = "${var.location}"
  resource_group_name       = "${var.resource_group_name}"
  network_security_group_id = "${var.security_group_id}"

  ip_configuration {
    name                          = "ip-${var.name}"
    subnet_id                     = "${azurerm_subnet.single.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${azurerm_public_ip.single.id}"
  }
}

resource "azurerm_virtual_machine" "single-vm" {
  count                         = "${var.enable_module ? 1 : 0}"
  name                          = "vm-${var.name}"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.single.id}"]
  vm_size                       = "Standard_DS3_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-${var.name}"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "os-profile-${var.name}"
    admin_username = "${var.ssh_user}"
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
      path     = "/home/${var.ssh_user}/.ssh/authorized_keys"
      key_data = "${file("${var.ssh_key_path}.pub")}"
    }
  }

  storage_data_disk {
    name              = "data-disk-${var.name}"
    managed_disk_type = "Standard_LRS"
    create_option     = "Empty"
    lun               = 0
    disk_size_gb      = "2048"
  }

  connection {
    type        = "ssh"
    host        = "${azurerm_public_ip.single.ip_address}"
    user        = "${var.ssh_user}"
    private_key = "${file("${var.ssh_key_path}")}"
    timeout     = "10s"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p bin-files",
    ]
  }

  provisioner "file" {
    source      = "${path.module}/bin-files/"
    destination = "bin-files"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo useradd -u 1100 git",
      "sudo useradd -u 1101 gitlab-psql",
      "sudo mv bin-files/* /usr/local/bin/",
      "sudo chmod 755 /usr/local/bin/*",
    ]
  }
}

resource "aws_route53_record" "single" {
  count   = "${var.enable_module ? 1 : 0}"
  zone_id = "${var.zone_id}"
  name    = "${terraform.workspace}.single.gitlab.com."
  type    = "A"
  ttl     = "300"
  records = ["${azurerm_public_ip.single.ip_address}"]
}

output "public_ip" {
  value = "${azurerm_public_ip.single.ip_address}"
}
