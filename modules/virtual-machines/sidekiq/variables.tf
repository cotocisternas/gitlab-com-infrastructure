variable "address_prefix" {}
variable "chef_repo_dir" {}
variable "chef_vaults" {}

variable "chef_vault_env" {
  default = "_default"
}

variable "chef_version" {}
variable "environment" {}
variable "first_user_password" {}
variable "first_user_username" {}
variable "gitlab_com_zone_id" {}
variable "location" {}
variable "resource_group_name" {}
variable "sidekiq_asap_count" {}
variable "sidekiq_asap_instance_type" {}
variable "sidekiq_besteffort_count" {}
variable "sidekiq_besteffort_instance_type" {}
variable "sidekiq_elasticsearch_count" {}
variable "sidekiq_elasticsearch_instance_type" {}
variable "sidekiq_pages_count" {}
variable "sidekiq_pages_instance_type" {}
variable "sidekiq_pipeline_count" {}
variable "sidekiq_pipeline_instance_type" {}
variable "sidekiq_pullmirror_count" {}
variable "sidekiq_pullmirror_instance_type" {}
variable "sidekiq_realtime_count" {}
variable "sidekiq_realtime_instance_type" {}
variable "sidekiq_traces_count" {}
variable "sidekiq_traces_instance_type" {}
variable "ssh_private_key" {}
variable "ssh_public_key" {}
variable "ssh_user" {}
variable "subnet_id" {}
variable "tier" {}
