# Supplied variables
variable "address_prefix" {
  description = "Subnet for vault VMs"
}

variable "count" {
  default     = "1"
  description = "Number of Vault VMs to provision"
}

variable "environment" {
  default     = "stg"
  description = "Vault VMs environment (stg|prd)"
}

variable "instance_type" {
  default     = "Standard_A0"
  description = "Instance type for Vault VMs"
}

variable "gitlab_net_zone_id" {
  description = "DNS zone to create records in"
}

variable "location" {
  default     = "East US 2"
  description = "Location of the VM, Azure region"
}

variable "resource_group_name" {
  description = "RG name for the Vault VMs"
}

variable "security_group_id" {
  description = "ID of security group applied to vault VMs"
}

variable "subnet_id" {
  description = "Subnet id Vault VMs located in"
}

variable "tier" {
  default     = "inf"
  description = "Vault tier"
}
