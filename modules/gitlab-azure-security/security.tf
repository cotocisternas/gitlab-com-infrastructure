variable "location" {}

variable "outbound_deny" {
  default = true
}

variable "resource_group_name" {}

resource "azurerm_network_security_group" "single" {
  count               = 1
  name                = "singleSecurityGroup"
  location            = "${var.location}"
  resource_group_name = "${var.resource_group_name}"

  security_rule {
    name                       = "in100"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "${var.inbound_wl[0]}"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "in101"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "${var.inbound_wl[1]}"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "in102"
    priority                   = 102
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "${var.inbound_wl[2]}"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "in103"
    priority                   = 103
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "${var.inbound_wl[3]}"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "in104"
    priority                   = 104
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "${var.inbound_wl[4]}"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "AllowInternalIn"
    priority                   = 490
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "10.0.0.0/8"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "DenyAll-Inbound"
    priority                   = 500
    direction                  = "Inbound"
    access                     = "Deny"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  #####################
  ###  START OUTBOUND
  #####################

  security_rule {
    name                       = "out-100"
    priority                   = 100
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[0]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out-101"
    priority                   = 101
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[1]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out-102"
    priority                   = 102
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[2]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out-103"
    priority                   = 103
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[3]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out-104"
    priority                   = 104
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[4]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out-105"
    priority                   = 105
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[5]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out-106"
    priority                   = 106
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[6]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out-107"
    priority                   = 107
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[7]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out-108"
    priority                   = 108
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[8]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out-109"
    priority                   = 109
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[9]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out-110"
    priority                   = 110
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[10]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out-111"
    priority                   = 111
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[11]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out-112"
    priority                   = 112
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[12]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out-113"
    priority                   = 113
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[13]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out-114"
    priority                   = 114
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[14]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out-115"
    priority                   = 115
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[15]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out-116"
    priority                   = 116
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[16]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out-117"
    priority                   = 117
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[17]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out-118"
    priority                   = 118
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[18]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out-119"
    priority                   = 119
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[19]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out-120"
    priority                   = 120
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[20]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out121"
    priority                   = 121
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[21]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out122"
    priority                   = 122
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[22]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out123"
    priority                   = 123
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[23]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out124"
    priority                   = 124
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[24]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out125"
    priority                   = 125
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[25]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out126"
    priority                   = 126
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[26]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out127"
    priority                   = 127
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[27]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out128"
    priority                   = 128
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[28]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out129"
    priority                   = 129
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[29]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out130"
    priority                   = 130
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[30]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out131"
    priority                   = 131
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[31]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out132"
    priority                   = 132
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[32]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out133"
    priority                   = 133
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[33]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out134"
    priority                   = 134
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[34]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out135"
    priority                   = 135
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[35]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out136"
    priority                   = 136
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[36]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out137"
    priority                   = 137
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[37]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out138"
    priority                   = 138
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[38]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out139"
    priority                   = 139
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[39]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out140"
    priority                   = 140
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[40]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out141"
    priority                   = 141
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[41]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out142"
    priority                   = 142
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[42]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out143"
    priority                   = 143
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[43]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out144"
    priority                   = 144
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[44]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out145"
    priority                   = 145
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[45]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out146"
    priority                   = 146
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[46]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out147"
    priority                   = 147
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[47]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out148"
    priority                   = 148
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[48]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out149"
    priority                   = 149
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[49]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out150"
    priority                   = 150
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[50]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out151"
    priority                   = 151
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[51]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out152"
    priority                   = 152
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[52]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out153"
    priority                   = 153
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[53]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out154"
    priority                   = 154
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[54]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out155"
    priority                   = 155
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[55]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out156"
    priority                   = 156
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[56]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out157"
    priority                   = 157
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[57]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out158"
    priority                   = 158
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[58]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out159"
    priority                   = 159
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[59]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out160"
    priority                   = 160
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[60]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out161"
    priority                   = 161
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[61]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out162"
    priority                   = 162
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[62]}"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "out163"
    priority                   = 163
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "${var.outbound_wl[63]}"
    source_address_prefix      = "*"
  }

  ## allow ntp for time synchronization

  security_rule {
    name                       = "out-300"
    priority                   = 300
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "UDP"
    source_port_range          = "*"
    destination_port_range     = "123"
    destination_address_prefix = "*"
    source_address_prefix      = "*"
  }

  ## allow outbound connections for syslog

  security_rule {
    name                       = "out-301"
    priority                   = 301
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "1514"
    destination_address_prefix = "*"
    source_address_prefix      = "*"
  }

  ## allow outbound connections for internal

  security_rule {
    name                       = "AllowInternalOut"
    priority                   = 390
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "10.0.0.0/8"
    source_address_prefix      = "*"
  }
  security_rule {
    name                       = "DenyAll-Outbound"
    priority                   = 400
    direction                  = "Outbound"
    access                     = "${var.outbound_deny ? "Deny" : "Allow"}"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

output "id" {
  value = "${azurerm_network_security_group.single.id}"
}
