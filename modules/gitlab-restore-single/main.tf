variable "disk_snapshot_date" {}
variable "restore_machine" {}
variable "location" {}
variable "resource_group_name" {}
variable "first_user_username" {}
variable "ssh_key_path" {}
variable "ssh_user" {}
variable "first_user_password" {}
variable "disk_subscription" {}
variable "backup_aws_access_key" {}
variable "backup_aws_secret_key" {}
variable "security_group_id" {}

variable "nfs_count" {
  default = 12
}

resource "azurerm_public_ip" "single" {
  name                         = "single-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
}

resource "azurerm_virtual_network" "single" {
  name                = "acctvn"
  address_space       = ["10.248.0.0/16"]
  location            = "${var.location}"
  resource_group_name = "${var.resource_group_name}"
}

resource "azurerm_resource_group" "single" {
  name     = "acceptanceTestResourceGroup1"
  location = "${var.location}"
}

resource "azurerm_subnet" "single" {
  name                 = "acctsub"
  resource_group_name  = "${var.resource_group_name}"
  virtual_network_name = "${azurerm_virtual_network.single.name}"
  address_prefix       = "10.248.2.0/24"
}

resource "azurerm_network_interface" "uploads" {
  name                = "uploads"
  location            = "${var.location}"
  resource_group_name = "${var.resource_group_name}"

  # network_security_group_id = "${azurerm_network_security_group.single.id}"
  network_security_group_id = "${var.security_group_id}"

  ip_configuration {
    name                          = "uploads"
    subnet_id                     = "${azurerm_subnet.single.id}"
    private_ip_address_allocation = "dynamic"

    #  public_ip_address_id          = "${azurerm_public_ip.single.id}"
  }
}

resource "azurerm_network_interface" "nfs" {
  count                     = "${var.nfs_count}"
  name                      = "nfs-${count.index}"
  location                  = "${var.location}"
  resource_group_name       = "${var.resource_group_name}"
  network_security_group_id = "${var.security_group_id}"

  ip_configuration {
    name                          = "nfs-${count.index}"
    subnet_id                     = "${azurerm_subnet.single.id}"
    private_ip_address_allocation = "dynamic"
  }
}

resource "azurerm_network_interface" "lfs1" {
  name                = "lfs1"
  location            = "${var.location}"
  resource_group_name = "${var.resource_group_name}"

  # network_security_group_id = "${azurerm_network_security_group.single.id}"
  network_security_group_id = "${var.security_group_id}"

  ip_configuration {
    name                          = "lfs1"
    subnet_id                     = "${azurerm_subnet.single.id}"
    private_ip_address_allocation = "dynamic"

    # public_ip_address_id          = "${azurerm_public_ip.single.id}"
  }
}

resource "azurerm_network_interface" "single" {
  name                = "acctni"
  location            = "${var.location}"
  resource_group_name = "${var.resource_group_name}"

  # network_security_group_id = "${azurerm_network_security_group.single.id}"
  network_security_group_id = "${var.security_group_id}"

  ip_configuration {
    name                          = "singleconfiguration1"
    subnet_id                     = "${azurerm_subnet.single.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${azurerm_public_ip.single.id}"
  }
}

resource "azurerm_managed_disk" "geo-osdisk" {
  name                 = "geo-osdisk"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Standard_LRS"
  create_option        = "Copy"
  source_resource_id   = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/GeoTestbed/providers/Microsoft.Compute/snapshots/os-disk-snapshot"
  disk_size_gb         = "300"
}

resource "azurerm_managed_disk" "single-01-datadisk-0" {
  name                 = "single-01-datadisk-0"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Copy"
  source_resource_id   = "/subscriptions/${var.disk_subscription}/resourceGroups/snapshots-${var.disk_snapshot_date}/providers/Microsoft.Compute/snapshots/${var.restore_machine}-datadisk-0-snap-${var.disk_snapshot_date}"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "single-01-datadisk-1" {
  name                 = "single-01-datadisk-1"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Copy"
  source_resource_id   = "/subscriptions/${var.disk_subscription}/resourceGroups/snapshots-${var.disk_snapshot_date}/providers/Microsoft.Compute/snapshots/${var.restore_machine}-datadisk-1-snap-${var.disk_snapshot_date}"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "single-01-datadisk-2" {
  name                 = "single-01-datadisk-2"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Copy"
  source_resource_id   = "/subscriptions/${var.disk_subscription}/resourceGroups/snapshots-${var.disk_snapshot_date}/providers/Microsoft.Compute/snapshots/${var.restore_machine}-datadisk-2-snap-${var.disk_snapshot_date}"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "single-01-datadisk-3" {
  name                 = "single-01-datadisk-3"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Copy"
  source_resource_id   = "/subscriptions/${var.disk_subscription}/resourceGroups/snapshots-${var.disk_snapshot_date}/providers/Microsoft.Compute/snapshots/${var.restore_machine}-datadisk-3-snap-${var.disk_snapshot_date}"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "single-01-datadisk-4" {
  name                 = "single-01-datadisk-4"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Copy"
  source_resource_id   = "/subscriptions/${var.disk_subscription}/resourceGroups/snapshots-${var.disk_snapshot_date}/providers/Microsoft.Compute/snapshots/${var.restore_machine}-datadisk-4-snap-${var.disk_snapshot_date}"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "single-01-datadisk-5" {
  name                 = "single-01-datadisk-5"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Copy"
  source_resource_id   = "/subscriptions/${var.disk_subscription}/resourceGroups/snapshots-${var.disk_snapshot_date}/providers/Microsoft.Compute/snapshots/${var.restore_machine}-datadisk-5-snap-${var.disk_snapshot_date}"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "single-01-datadisk-6" {
  name                 = "single-01-datadisk-6"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Copy"
  source_resource_id   = "/subscriptions/${var.disk_subscription}/resourceGroups/snapshots-${var.disk_snapshot_date}/providers/Microsoft.Compute/snapshots/${var.restore_machine}-datadisk-6-snap-${var.disk_snapshot_date}"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "single-01-datadisk-7" {
  name                 = "single-01-datadisk-7"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Copy"
  source_resource_id   = "/subscriptions/${var.disk_subscription}/resourceGroups/snapshots-${var.disk_snapshot_date}/providers/Microsoft.Compute/snapshots/${var.restore_machine}-datadisk-7-snap-${var.disk_snapshot_date}"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "single-01-datadisk-8" {
  name                 = "single-01-datadisk-8"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Copy"
  source_resource_id   = "/subscriptions/${var.disk_subscription}/resourceGroups/snapshots-${var.disk_snapshot_date}/providers/Microsoft.Compute/snapshots/${var.restore_machine}-datadisk-8-snap-${var.disk_snapshot_date}"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "single-01-datadisk-9" {
  name                 = "single-01-datadisk-9"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Copy"
  source_resource_id   = "/subscriptions/${var.disk_subscription}/resourceGroups/snapshots-${var.disk_snapshot_date}/providers/Microsoft.Compute/snapshots/${var.restore_machine}-datadisk-9-snap-${var.disk_snapshot_date}"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "single-01-datadisk-10" {
  name                 = "single-01-datadisk-10"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Copy"
  source_resource_id   = "/subscriptions/${var.disk_subscription}/resourceGroups/snapshots-${var.disk_snapshot_date}/providers/Microsoft.Compute/snapshots/${var.restore_machine}-datadisk-10-snap-${var.disk_snapshot_date}"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "single-01-datadisk-11" {
  name                 = "single-01-datadisk-11"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Copy"
  source_resource_id   = "/subscriptions/${var.disk_subscription}/resourceGroups/snapshots-${var.disk_snapshot_date}/providers/Microsoft.Compute/snapshots/${var.restore_machine}-datadisk-11-snap-${var.disk_snapshot_date}"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "single-01-datadisk-12" {
  name                 = "single-01-datadisk-12"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Copy"
  source_resource_id   = "/subscriptions/${var.disk_subscription}/resourceGroups/snapshots-${var.disk_snapshot_date}/providers/Microsoft.Compute/snapshots/${var.restore_machine}-datadisk-12-snap-${var.disk_snapshot_date}"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "single-01-datadisk-13" {
  name                 = "single-01-datadisk-13"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Copy"
  source_resource_id   = "/subscriptions/${var.disk_subscription}/resourceGroups/snapshots-${var.disk_snapshot_date}/providers/Microsoft.Compute/snapshots/${var.restore_machine}-datadisk-13-snap-${var.disk_snapshot_date}"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "single-01-datadisk-14" {
  name                 = "single-01-datadisk-14"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Copy"
  source_resource_id   = "/subscriptions/${var.disk_subscription}/resourceGroups/snapshots-${var.disk_snapshot_date}/providers/Microsoft.Compute/snapshots/${var.restore_machine}-datadisk-14-snap-${var.disk_snapshot_date}"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "single-01-datadisk-15" {
  name                 = "single-01-datadisk-15"
  location             = "${var.location}"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Copy"
  source_resource_id   = "/subscriptions/${var.disk_subscription}/resourceGroups/snapshots-${var.disk_snapshot_date}/providers/Microsoft.Compute/snapshots/${var.restore_machine}-datadisk-15-snap-${var.disk_snapshot_date}"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "lfs1" {
  name                          = "lfs1"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.lfs1.id}"]
  vm_size                       = "Standard_DS3_v2"
  delete_os_disk_on_termination = true

  os_profile {
    computer_name  = "lfs1"
    admin_username = "terraform"
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
      path     = "/home/terraform/.ssh/authorized_keys"
      key_data = "${file("${var.ssh_key_path}.pub")}"
    }
  }

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-lfs1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
}

resource "azurerm_virtual_machine" "uploads" {
  name                          = "uploads"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.uploads.id}"]
  vm_size                       = "Standard_DS3_v2"
  delete_os_disk_on_termination = true

  os_profile {
    computer_name  = "uploads"
    admin_username = "terraform"
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
      path     = "/home/terraform/.ssh/authorized_keys"
      key_data = "${file("${var.ssh_key_path}.pub")}"
    }
  }

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-uploads"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
}

resource "azurerm_virtual_machine" "nfs" {
  count                         = "${var.nfs_count}"
  name                          = "nfs-${count.index + 1}"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs.*.id[count.index]}"]
  vm_size                       = "Standard_DS3_v2"
  delete_os_disk_on_termination = true

  os_profile {
    computer_name  = "nfs-${count.index +1}"
    admin_username = "terraform"
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
      path     = "/home/terraform/.ssh/authorized_keys"
      key_data = "${file("${var.ssh_key_path}.pub")}"
    }
  }

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-${count.index + 1}"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
}

resource "azurerm_virtual_machine" "single-vm" {
  name                          = "single-testbed"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.single.id}"]
  vm_size                       = "Standard_DS4_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name          = "osdisk-single-testbed"
    caching       = "ReadWrite"
    create_option = "Attach"

    # managed_disk_id   = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/GeoTestbed/providers/Microsoft.Compute/snapshots/os-disk-snapshot"
    # managed_disk_id   = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/GEOTESTBED/providers/Microsoft.Compute/disks/osdisk-single-testbed"
    managed_disk_id = "${azurerm_managed_disk.geo-osdisk.id}"

    disk_size_gb = "${azurerm_managed_disk.geo-osdisk.disk_size_gb}"
  }

  #  os_profile {
  #    computer_name  = "single-testbed"
  #    admin_username = "${var.first_user_username}"
  #    admin_password = "${var.first_user_password}"
  #  }


  #  os_profile_linux_config {
  #    disable_password_authentication = false
  #  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.single-01-datadisk-0.name}"
    managed_disk_id = "${azurerm_managed_disk.single-01-datadisk-0.id}"
    disk_size_gb    = "${azurerm_managed_disk.single-01-datadisk-0.disk_size_gb}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }
  storage_data_disk {
    name            = "${azurerm_managed_disk.single-01-datadisk-1.name}"
    managed_disk_id = "${azurerm_managed_disk.single-01-datadisk-1.id}"
    disk_size_gb    = "${azurerm_managed_disk.single-01-datadisk-1.disk_size_gb}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }
  storage_data_disk {
    name            = "${azurerm_managed_disk.single-01-datadisk-2.name}"
    managed_disk_id = "${azurerm_managed_disk.single-01-datadisk-2.id}"
    disk_size_gb    = "${azurerm_managed_disk.single-01-datadisk-2.disk_size_gb}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }
  storage_data_disk {
    name            = "${azurerm_managed_disk.single-01-datadisk-3.name}"
    managed_disk_id = "${azurerm_managed_disk.single-01-datadisk-3.id}"
    disk_size_gb    = "${azurerm_managed_disk.single-01-datadisk-3.disk_size_gb}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }
  storage_data_disk {
    name            = "${azurerm_managed_disk.single-01-datadisk-4.name}"
    managed_disk_id = "${azurerm_managed_disk.single-01-datadisk-4.id}"
    disk_size_gb    = "${azurerm_managed_disk.single-01-datadisk-4.disk_size_gb}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }
  storage_data_disk {
    name            = "${azurerm_managed_disk.single-01-datadisk-5.name}"
    managed_disk_id = "${azurerm_managed_disk.single-01-datadisk-5.id}"
    disk_size_gb    = "${azurerm_managed_disk.single-01-datadisk-5.disk_size_gb}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }
  storage_data_disk {
    name            = "${azurerm_managed_disk.single-01-datadisk-6.name}"
    managed_disk_id = "${azurerm_managed_disk.single-01-datadisk-6.id}"
    disk_size_gb    = "${azurerm_managed_disk.single-01-datadisk-6.disk_size_gb}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }
  storage_data_disk {
    name            = "${azurerm_managed_disk.single-01-datadisk-7.name}"
    managed_disk_id = "${azurerm_managed_disk.single-01-datadisk-7.id}"
    disk_size_gb    = "${azurerm_managed_disk.single-01-datadisk-7.disk_size_gb}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }
  storage_data_disk {
    name            = "${azurerm_managed_disk.single-01-datadisk-8.name}"
    managed_disk_id = "${azurerm_managed_disk.single-01-datadisk-8.id}"
    disk_size_gb    = "${azurerm_managed_disk.single-01-datadisk-8.disk_size_gb}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }
  storage_data_disk {
    name            = "${azurerm_managed_disk.single-01-datadisk-9.name}"
    managed_disk_id = "${azurerm_managed_disk.single-01-datadisk-9.id}"
    disk_size_gb    = "${azurerm_managed_disk.single-01-datadisk-9.disk_size_gb}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }
  storage_data_disk {
    name            = "${azurerm_managed_disk.single-01-datadisk-10.name}"
    managed_disk_id = "${azurerm_managed_disk.single-01-datadisk-10.id}"
    disk_size_gb    = "${azurerm_managed_disk.single-01-datadisk-10.disk_size_gb}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }
  storage_data_disk {
    name            = "${azurerm_managed_disk.single-01-datadisk-11.name}"
    managed_disk_id = "${azurerm_managed_disk.single-01-datadisk-11.id}"
    disk_size_gb    = "${azurerm_managed_disk.single-01-datadisk-11.disk_size_gb}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }
  storage_data_disk {
    name            = "${azurerm_managed_disk.single-01-datadisk-12.name}"
    managed_disk_id = "${azurerm_managed_disk.single-01-datadisk-12.id}"
    disk_size_gb    = "${azurerm_managed_disk.single-01-datadisk-12.disk_size_gb}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }
  storage_data_disk {
    name            = "${azurerm_managed_disk.single-01-datadisk-13.name}"
    managed_disk_id = "${azurerm_managed_disk.single-01-datadisk-13.id}"
    disk_size_gb    = "${azurerm_managed_disk.single-01-datadisk-13.disk_size_gb}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }
  storage_data_disk {
    name            = "${azurerm_managed_disk.single-01-datadisk-14.name}"
    managed_disk_id = "${azurerm_managed_disk.single-01-datadisk-14.id}"
    disk_size_gb    = "${azurerm_managed_disk.single-01-datadisk-14.disk_size_gb}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }
  storage_data_disk {
    name            = "${azurerm_managed_disk.single-01-datadisk-15.name}"
    managed_disk_id = "${azurerm_managed_disk.single-01-datadisk-15.id}"
    disk_size_gb    = "${azurerm_managed_disk.single-01-datadisk-15.disk_size_gb}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
  connection {
    type     = "ssh"
    host     = "${azurerm_public_ip.single.ip_address}"
    user     = "${var.first_user_username}"
    password = "${var.first_user_password}"
    timeout  = "10s"
  }
  provisioner "file" {
    source      = "${path.module}/files/bootstrap.bash"
    destination = "/tmp/bootstrap.bash"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod 700 /tmp/bootstrap.bash",
      "sudo /tmp/bootstrap.bash ${var.backup_aws_access_key} ${var.backup_aws_secret_key}",
    ]
  }
}

output "public_ip" {
  value = "${azurerm_public_ip.single.ip_address}"
}
