data "null_data_source" "suffixes" {
  inputs = {
    dns_suffix_with_tier         = "${join(".", list(var.tier, var.environment == "prod" ? "prd" : var.environment, var.domain_name))}"
    dns_suffix_without_tier      = "${join(".", list(var.environment == "prod" ? "prd" : var.environment, var.domain_name))}"
    resource_suffix_with_tier    = "${join("-", list(var.tier, var.environment == "prod" ? "prd" : var.environment, var.domain_name))}"
    resource_suffix_without_tier = "${join("-", list(var.environment == "prod" ? "prd" : var.environment, var.domain_name))}"
  }
}

data "null_data_source" "full_domain" {
  inputs = {
    dns_domain      = "${var.use_tier_in_suffix ? data.null_data_source.suffixes.outputs["dns_suffix_with_tier"] : data.null_data_source.suffixes.outputs["dns_suffix_without_tier"]}"
    resource_domain = "${var.use_tier_in_suffix ? data.null_data_source.suffixes.outputs["resource_suffix_with_tier"] : data.null_data_source.suffixes.outputs["resource_suffix_without_tier"]}"
  }
}

resource "azurerm_availability_set" "availability_set" {
  name                         = "${format("%s-%s", var.name, var.environment)}"
  location                     = "${var.location}"
  managed                      = true
  platform_update_domain_count = 20
  platform_fault_domain_count  = 3
  resource_group_name          = "${var.resource_group_name}"
}

resource "azurerm_network_interface" "nic" {
  count                   = "${var.count}"
  name                    = "${format("%s-%02d.%s", var.name, count.index + 1, data.null_data_source.full_domain.outputs["dns_domain"])}"
  internal_dns_name_label = "${format("%s-%02d-%v-%v", var.name, count.index + 1, var.tier, "gitlab-net")}"                               # sorry
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "${format("%s-%02d.%s", var.name, count.index + 1, data.null_data_source.full_domain.outputs["dns_domain"])}"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${azurerm_public_ip.public_ip.id}"
  }
}

resource "azurerm_public_ip" "public_ip" {
  count                        = "${var.use_public_ip ? 1 : 0}"
  name                         = "${format("%s-%02d-%s-public-ip", var.name, count.index + 1, var.environment)}"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
}

resource "aws_route53_record" "public_record" {
  count   = "${var.use_public_ip ? var.count : 0}"
  zone_id = "${var.gitlab_net_zone_id}"
  name    = "${format("%s.%s", var.public_name, var.domain_name)}"
  type    = "A"
  ttl     = "300"
  records = ["${azurerm_public_ip.public_ip.ip_address}"]
}

resource "aws_route53_record" "dns_record" {
  count   = "${var.use_dns ? var.count : 0}"
  zone_id = "${var.gitlab_net_zone_id}"
  name    = "${format("%s-%02d.%s", var.name, count.index + 1, var.environment)}"
  type    = "A"
  ttl     = "300"
  records = ["${azurerm_network_interface.nic.*.private_ip_address[count.index]}"]
}

data "template_file" "chef_bootstrap" {
  count    = "${var.count}"
  template = "${file("${path.root}/../../templates/chef-bootstrap-ssh-keys.tpl")}"

  vars {
    chef_repo_dir   = "${var.chef_repo_dir}"
    chef_vaults     = "${var.chef_vaults}"
    chef_vault_env  = "${var.chef_vault_env}"
    chef_version    = "${var.chef_version}"
    environment     = "${var.environment}"
    hostname        = "${format("%s-%02d.%s", var.name, count.index + 1, data.null_data_source.full_domain.outputs["dns_domain"])}"
    ip_address      = "${azurerm_network_interface.nic.*.private_ip_address[count.index]}"
    ssh_private_key = "${var.ssh_private_key}"
    ssh_user        = "${var.ssh_user}"
  }
}

resource "azurerm_virtual_machine" "vm" {
  count                         = "${var.count}"
  name                          = "${format("%s-%02d.%s", var.name, count.index + 1, data.null_data_source.full_domain.outputs["dns_domain"])}"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  availability_set_id           = "${azurerm_availability_set.availability_set.id}"
  network_interface_ids         = ["${azurerm_network_interface.nic.*.id[count.index]}"]
  primary_network_interface_id  = "${azurerm_network_interface.nic.*.id[count.index]}"
  vm_size                       = "${var.instance_type}"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${format("osdisk-%s-%02d-%s", var.name, count.index + 1, var.environment)}"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  # storage_data_disk {
  #   name              = "${format("data-%s-%02d-%s", var.name, count.index + 1, var.environment)}"
  #   managed_disk_type = "Premium_LRS"
  #   create_option     = "Empty"
  #   lun               = 0
  #   disk_size_gb      = "${var.datadisk_size}"
  # }

  os_profile {
    computer_name  = "${format("%s-%02d.%s", var.name, count.index + 1, data.null_data_source.full_domain.outputs["dns_domain"])}"
    admin_username = "${var.ssh_user}"
  }
  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys = {
      path     = "/home/${var.ssh_user}/.ssh/authorized_keys"
      key_data = "${file("${var.ssh_public_key}")}"
    }
  }
  provisioner "local-exec" {
    command = "${data.template_file.chef_bootstrap.*.rendered[count.index]}"
  }
  provisioner "local-exec" {
    when    = "destroy"
    command = "cd ${var.chef_repo_dir}; bundle exec knife node delete ${format("%s-%02d.%s", var.name, count.index + 1, data.null_data_source.full_domain.outputs["dns_domain"])} -y; bundle exec knife client delete ${format("%s-%02d.%s", var.name, count.index + 1, data.null_data_source.full_domain.outputs["dns_domain"])} -y"
  }
  provisioner "remote-exec" {
    inline = ["nohup bash -c 'sudo chef-client &'"]

    connection {
      type        = "ssh"
      host        = "${azurerm_network_interface.nic.*.private_ip_address[count.index]}"
      user        = "${var.ssh_user}"
      private_key = "${file("${var.ssh_private_key}")}"
      timeout     = "10s"
    }
  }
}

output "private_ip" {
  value = "${azurerm_network_interface.nic.private_ip_address}"
}

output "public_ip" {
  value = "${azurerm_public_ip.public_ip.ip_address}"
}
